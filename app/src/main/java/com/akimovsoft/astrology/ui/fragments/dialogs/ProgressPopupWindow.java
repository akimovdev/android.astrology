package com.akimovsoft.astrology.ui.fragments.dialogs;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.akimovsoft.astrology.R;


public class ProgressPopupWindow extends PopupWindow {

    private ProgressPopupWindow(View contentView, int width, int height, boolean focusable) {
        super(contentView, width, height, focusable);

        setOutsideTouchable(true);
        //showAtLocation(contentView, Gravity.CENTER, 0, 0);
        setFocusable(true);
    }


    public static ProgressPopupWindow create(Activity context) {

        DisplayMetrics metrics = getDisplaySize(context);

        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.dialog_progress, (ViewGroup) context.findViewById(R.id.popup_element));

        ProgressPopupWindow  popup = new ProgressPopupWindow(layout, metrics.widthPixels, metrics.heightPixels, true);

        return popup;
    }

    public void showAtLocation(int gravity, int x, int y) {
        super.showAtLocation(getContentView(), gravity, x, y);
    }

    public static DisplayMetrics getDisplaySize(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        return displayMetrics ;
    }

}
