package com.akimovsoft.astrology.model.requests;

import android.util.Log;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class UserRequest extends StringRequest {

    private Map<String, String> params = new HashMap<String, String>();

    private UserRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    private UserRequest(int method, String url, Map<String, String> params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.params.putAll(params);
    }

    public static UserRequest auth(String email, String password, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("email", email);
        params.put("password", password);

        String url = Constants.SERVER_PRODUCTION_API +"users/auth";

        return new UserRequest(Method.POST, Constants.SERVER_PRODUCTION_API +"users/auth", params, listener, errorListener);
    }


    public static JsonObjectRequest authJson() {

        JSONObject jsonBodyObj = new JSONObject();
        String url = "http://sharisse.beget.tech/api/users/auth";
        try{
            jsonBodyObj.put("email", "akimov.dev@gmail.com");
            jsonBodyObj.put("password", "qwerty");
        }catch (JSONException e){
            e.printStackTrace();
        }

        final String requestBody = jsonBodyObj.toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>(){
            @Override    public void onResponse(JSONObject response) {
                Log.i("Response",String.valueOf(response));
            }
        }, new Response.ErrorListener() {
            @Override    public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override    public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                //headers.put("Authorization", Constants.DEBUG_TOKEN);
                headers.put("Authorization", AstrologyApplication.getInstance().getToken());

                return headers;
            }


            @Override    public byte[] getBody() {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            requestBody, "utf-8");
                    return null;
                }
            }
        };

        return jsonObjectRequest;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Accept", "application/json");
        //params.put("Authorization", Constants.DEBUG_TOKEN);
        params.put("Authorization", AstrologyApplication.getInstance().getToken());
        params.put("Content-Type", "application/json");
        return params;
    }

    @Override
    public String getBodyContentType() {
        JSONObject auth = new JSONObject();
        try {
            auth.put("email", "akimov.dev@gmail.com");
            auth.put("password", "qwerty");

            String str = auth.toString();
            str.toString();

            return str;

        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        return super.getBodyContentType();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return super.getBody();
    }

    /*
    public static JsonObjectRequest debugRequest() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", "akimov.dev@gmail.com");
        params.put("password", "qwerty");

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                "http://sharisse.beget.tech/api/users/auth", new JsonObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("JSONPost", response.toString());
                        //pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("JSONPost", "Error: " + error.getMessage());
                //pDialog.hide();
            }
        });


        return jsonObjReq;
    }*/
}
