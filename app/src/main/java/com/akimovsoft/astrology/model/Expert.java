package com.akimovsoft.astrology.model;

import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.model.entity.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Expert {

    @Expose
    private int price;

    @Expose
    private float rating;

    @Expose
    private float experience;

    @Expose
    private User user;

    @Expose
    @SerializedName("dialog")
    private Dialog dialog;


    @Expose
    @SerializedName("id")
    private int serverID;

    @Expose
    @SerializedName("feedback_count")
    private int feedbackCount;

    @Expose
    @SerializedName("category")
    private Service service;

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public String getName() {
        return user.getName();
    }

    public float getExperience() {
        return experience;
    }

    public int getPrice() {
        return price;
    }

    public float getRating() {
        return rating;
    }

    public Service getService() {
        return service;
    }

    public int getFeedbackCount() {
        return feedbackCount;
    }

    public int getServerID() {
        return serverID;
    }

    public User getUser() {
        return user;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public boolean hasDialog() {
        return dialog != null;
    }
}
