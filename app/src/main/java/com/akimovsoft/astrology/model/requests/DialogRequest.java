package com.akimovsoft.astrology.model.requests;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class DialogRequest extends StringRequest {

    public DialogRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public static DialogRequest loadDialogs(long userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new DialogRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"dialogs/user/"+userID, listener, errorListener);
    }

    public static DialogRequest loadAll(long userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new DialogRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"dialogs/all/user/"+userID, listener, errorListener);
    }

    public static DialogRequest loadBid(long userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new DialogRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"dialogs/bids/user/"+userID, listener, errorListener);
    }

    public static DialogRequest loadActive(long userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new DialogRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"dialogs/active/user/"+userID, listener, errorListener);
    }

    public static DialogRequest loadClosed(long userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new DialogRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"dialogs/closed/user/"+userID, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Accept", "application/json");
        params.put("Content-Type", "application/json");
        params.put("Authorization", AstrologyApplication.getInstance().getToken());
        //params.put("Authorization", Constants.DEBUG_TOKEN);
        return params;
    }
}
