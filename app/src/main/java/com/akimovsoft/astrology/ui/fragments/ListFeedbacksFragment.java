package com.akimovsoft.astrology.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Feedback;
import com.akimovsoft.astrology.model.requests.FeedbackRequest;
import com.akimovsoft.astrology.ui.activities.FeedbackActivity;
import com.akimovsoft.astrology.ui.adapters.FeedbackAdapter;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;


public class ListFeedbacksFragment extends Fragment {
    private static final String TAG = "LIST_FEEDBACKS_FRAGMENT";

    private static final String ARG_EXPERT_SERVER_ID = "expert_server_id";
    private long expertServerID = 2;
    private RecyclerView rvFeedbacks;
    private RequestQueue queue;
    private ProgressBar mProgressBar;
    private TextView mEmptyListTV;

    public ListFeedbacksFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ListFeedbacksFragment newInstance(long expertServerID) {
        ListFeedbacksFragment fragment = new ListFeedbacksFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_EXPERT_SERVER_ID, expertServerID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            expertServerID = getArguments().getLong(ARG_EXPERT_SERVER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_feedback, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgressBar = view.findViewById(R.id.progress_bar);
        mEmptyListTV = view.findViewById(R.id.tv_empty_list);

        rvFeedbacks = view.findViewById(R.id.rv_data);
        rvFeedbacks.setLayoutManager(new LinearLayoutManager(view.getContext()));

        ((FeedbackActivity)getActivity()).setToolBarTitle("Отзывы");

        requestLoadFeedbacks();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    private void requestLoadFeedbacks() {

        queue = Volley.newRequestQueue(getContext());

        FeedbackRequest feedbackRequest = FeedbackRequest.loadFeedbacks(expertServerID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressBar.setVisibility(View.GONE);
                //rvServices.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    parseResponse( jsonResponse.getString("data") );
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String str = null;
                //Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        feedbackRequest.setTag(TAG);
        queue.add(feedbackRequest);
    }

    private void parseResponse(String jsonCategories) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        List<Feedback> list = gson.fromJson(jsonCategories, new TypeToken<ArrayList<Feedback>>() {}.getType());

        if (list.isEmpty()) {
            mEmptyListTV.setVisibility(View.VISIBLE);
        }

        FeedbackAdapter expertsAdapter = FeedbackAdapter.createAdapter(list);
        rvFeedbacks.setAdapter(expertsAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
