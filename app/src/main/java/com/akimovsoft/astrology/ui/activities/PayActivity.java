package com.akimovsoft.astrology.ui.activities;

import android.content.Context;
import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Expert;
import com.akimovsoft.astrology.ui.fragments.PayFragment;

public class PayActivity extends BaseActivity {

    private TextView mToolBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_base);
        setContentView(R.layout.activity_custom_title);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }


        mToolBarTitle = findViewById(R.id.toolbar_title);
        mToolBarTitle.setText(getString(R.string.title_pay));

        if (savedInstanceState == null) {
            replaceFragment(PayFragment.newInstance(expert));
        }
    }

    private static Expert expert;

    public static Intent createIntent(Context context, Expert expert) {
        PayActivity.expert = expert;
        return new Intent(context, PayActivity.class);
    }
}
