package com.akimovsoft.astrology.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.fragments.FeedbackFragment;
import com.akimovsoft.astrology.ui.fragments.ListFeedbacksFragment;

public class FeedbackActivity extends BaseActivity {

    private final static int START_LIST = 0;
    private final static int START_CREATION = 1;
    private final static int START_ERROR = -1;



    private TextView mToolBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_title);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolBarTitle = findViewById(R.id.toolbar_title);
        mToolBarTitle.setText(getString(R.string.close_bid));


        if (savedInstanceState == null) {
            //long expertServerID = getIntent().getLongExtra(Constants.EXTRA_EXPERT_SERVER_ID, -1);

            //if (expertServerID == -1)
                //throw new Error("Not found extra expert server id");

            //replaceFragment(ListFeedbacksFragment.newInstanceHistory(expertServerID));

            int startMode = getIntent().getIntExtra(Constants.EXTRA_START_MODE, START_ERROR);

            if (startMode == START_LIST) {
                onListCreate();
            } else if (startMode == START_CREATION) {
                onCreationCreate();
            }
        }
    }

    private void onListCreate() {
        long expertServerID = getIntent().getLongExtra(Constants.EXTRA_EXPERT_SERVER_ID, -1);

        if (expertServerID == -1)
            throw new Error("Not found extra expert server id");

        replaceFragment(ListFeedbacksFragment.newInstance(expertServerID));
    }

    private void onCreationCreate() {

        long expertID = getIntent().getLongExtra(Constants.EXTRA_EXPERT_SERVER_ID, -1);
        long dialogID = getIntent().getLongExtra(Constants.EXTRA_DIALOG_SERVER_ID, -1);
        long userID = getIntent().getLongExtra(Constants.EXTRA_USER_SERVER_ID, -1);

        if (expertID == -1 || dialogID == -1 || userID == -1)
            throw new Error("Lost one or more extra data");

        replaceFragment(FeedbackFragment.newInstance(expertID, dialogID, userID));
    }

    public void setToolBarTitle(String title) {
        mToolBarTitle.setText(title);
    }

    public static Intent newListFeedbackIntent(Context context, long expertServerID) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra(Constants.EXTRA_START_MODE, START_LIST);
        intent.putExtra(Constants.EXTRA_EXPERT_SERVER_ID, expertServerID);
        return intent;
    }

    public static Intent newCreationFeedbackIntent(Context context, long expertID, long dialogID, long userID) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra(Constants.EXTRA_START_MODE, START_CREATION);
        intent.putExtra(Constants.EXTRA_EXPERT_SERVER_ID, expertID);
        intent.putExtra(Constants.EXTRA_DIALOG_SERVER_ID, dialogID);
        intent.putExtra(Constants.EXTRA_USER_SERVER_ID, userID);
        //intent.putExtra(Constants.EXTRA_EXPERT_SERVER_ID, expertServerID);
        return intent;
    }
}
