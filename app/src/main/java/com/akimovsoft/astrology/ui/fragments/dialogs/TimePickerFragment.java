package com.akimovsoft.astrology.ui.fragments.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.adapters.FiltersAdapter;
import com.akimovsoft.astrology.view.DividerItemDecorator;

import java.util.ArrayList;
import java.util.List;

public class TimePickerFragment extends DialogFragment {

    private FiltersAdapter.OnItemListener listener;
    TimePicker mPicker;
    TimePicker.OnTimeChangedListener onTimeChangedListener;

    public TimePickerFragment() {
        // Required empty public constructor
    }

    public static DialogFragment newInstance(FiltersAdapter.OnItemListener listener) {
        TimePickerFragment fragment = new TimePickerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.listener = listener;
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_timepicker_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPicker = view.findViewById(R.id.timePicker);
        mPicker.setIs24HourView(true);
        mPicker.setOnTimeChangedListener(onTimeChangedListener);

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    public void setOnTimeChangedListener(TimePicker.OnTimeChangedListener onTimeChangedListener) {
        this.onTimeChangedListener = onTimeChangedListener;
    }

    public class FilterEntity {

        public final static String INCREASE_PRICE = "price";
        public final static String DESCENDING_PRICE = "-price";
        public final static String HIGH_RATING = "rating";
        public final static String HIGH_EXPERIENCE = "experience";
        public final static String SPEED = "speed";
        public final static String DISCIPLE = "disciple";

        private int id;
        private String name;
        private String sort;

        public FilterEntity(String name, String sort) {
            this.name = name;
            this.sort = sort;
        }

        public String getName() {
            return name;
        }

        public String getSort() {
            return sort;
        }
    }
}