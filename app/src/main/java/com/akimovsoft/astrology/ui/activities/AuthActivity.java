package com.akimovsoft.astrology.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.fragments.WelcomeFragment;
import com.akimovsoft.astrology.ui.fragments.PersonalDataFragment;
import com.akimovsoft.astrology.ui.fragments.SignInFragment;
import com.akimovsoft.astrology.ui.fragments.SignUpFragment;
import com.akimovsoft.astrology.ui.fragments.SignUpNicknameFragment;

public class AuthActivity extends BaseActivity implements SignInFragment.OnSignInListener,
                                                            SignUpFragment.OnSignUpListener,
                                                            SignUpNicknameFragment.OnNicknameListener,
                                                            PersonalDataFragment.OnPersonalDataListener {

    TextView mToolBarTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mToolBarTV = findViewById(R.id.toolbar_title);
        mToolBarTV.setVisibility(View.GONE);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        replaceFragment(WelcomeFragment.newInstance());
        //replaceFragment(PersonalDataFragment.newInstance());
        //replaceFragment(SignUpNicknameFragment.newInstance());
    }

    public void setToolBarTitle(String title) {
        this.mToolBarTV.setText(title);
    }

    public void isToolBarTitleVisible(boolean isToolBarTitleVisible) {
        int visibility = isToolBarTitleVisible ? View.VISIBLE : View.GONE;
        this.mToolBarTV.setVisibility(visibility);
    }

    @Override
    public void onSignInSuccessful() {
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onSignUpSuccessful() {
        replaceFragment(SignUpNicknameFragment.newInstance());
    }

    @Override
    public void onPutNicknameSuccessful() {
        replaceFragment(PersonalDataFragment.newInstance());
    }

    @Override
    public void onPersonalDataSuccessful() {
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }
}
