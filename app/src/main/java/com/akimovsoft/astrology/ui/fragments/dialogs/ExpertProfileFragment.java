package com.akimovsoft.astrology.ui.fragments.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Expert;
import com.akimovsoft.astrology.model.requests.ExpertRequest;
import com.akimovsoft.astrology.ui.activities.FeedbackActivity;
import com.akimovsoft.astrology.ui.activities.MessagesActivity;
import com.akimovsoft.astrology.ui.activities.PayActivity;
import com.akimovsoft.astrology.ui.adapters.ExpertsAdapter;
import com.akimovsoft.astrology.view.DividerItemDecorator;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class ExpertProfileFragment extends DialogFragment {
    private static final String TAG = "PROFILE_FRAGMENT";

    private Expert mExpert;
    private RequestQueue mQueue;
    private RecyclerView expertsRV;

    public ExpertProfileFragment() {
        // Required empty public constructor
    }

    public static ExpertProfileFragment newInstance(Expert expert) {
        ExpertProfileFragment fragment = new ExpertProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.mExpert = expert;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CircleImageView avatarCIV =  view.findViewById(R.id.iv_avatar);
        Button feedbackBtn = view.findViewById(R.id.btn_feedback);
        Button nextBtn = view.findViewById(R.id.btn_next);
        TextView usernameTV = view.findViewById(R.id.tv_name);
        TextView experienceTV = view.findViewById(R.id.tv_experience);
        TextView serviceTV = view.findViewById(R.id.tv_main_service);
        TextView priceTV = view.findViewById(R.id.tv_main_price);

        expertsRV = view.findViewById(R.id.rv_experts);
        expertsRV.setLayoutManager(new LinearLayoutManager(view.getContext()));

        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        expertsRV.addItemDecoration(dividerItemDecoration);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(mExpert.getUser().getUrlAvatar(), avatarCIV);

        usernameTV.setText(mExpert.getUser().getName());
        serviceTV.setText(mExpert.getService().getName());
        priceTV.setText(mExpert.getPrice()+" ₽");
        experienceTV.setText(getString(R.string.experience)+" "+String.valueOf(mExpert.getExperience()) + " " + getString(R.string.years));
        feedbackBtn.setText(String.valueOf(mExpert.getFeedbackCount()) + " " + getString(R.string.has_feedback));

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mExpert.hasDialog()) {
                    Intent intent = MessagesActivity.createIntent(getContext(), mExpert.getDialog(), AstrologyApplication.getInstance().getCurrentUser(), mExpert.getUser());
                    startActivity(intent);
                    return;
                }

                startActivity(PayActivity.createIntent(getActivity(), mExpert));
            }
        });

        feedbackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(FeedbackActivity.newListFeedbackIntent(getActivity(), mExpert.getServerID()));
            }
        });

        requestLoadExperts();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mQueue != null) {
            mQueue.cancelAll(TAG);
        }
    }

    private void requestLoadExperts() {
        mQueue = Volley.newRequestQueue(getContext());

        ExpertRequest expertRequest = ExpertRequest.loadExpertsByUser(mExpert.getUser().getServerID(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //progressBar.setVisibility(View.GONE);
                //rvServices.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    parseResponse( jsonResponse.getString("data") );
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String str = null;
                //Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        expertRequest.setTag(TAG);
        mQueue.add(expertRequest);
    }

    private void parseResponse(String jsonExperts) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        List<Expert> list = gson.fromJson(jsonExperts, new TypeToken<ArrayList<Expert>>() {}.getType());

        ExpertsAdapter expertsAdapter = ExpertsAdapter.createMinDataAdapter(list, null);
        expertsRV.setAdapter(expertsAdapter);
    }

    public Expert getExpert() {
        return mExpert;
    }

    public interface OnExpertProfileFragmentListener {
        void onDestroyExpertProfileFragment();
    }
}
