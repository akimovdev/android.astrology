package com.akimovsoft.astrology.model.requests;

import androidx.annotation.Nullable;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.model.entity.User;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class MessageJsonRequest extends JsonObjectRequest {

    private MessageJsonRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public static MessageJsonRequest createSendMessageRequest(long dialogID, String message, String imageBase64, User userSender, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener){

        String url = Constants.SERVER_PRODUCTION_API +"messages";

        JSONObject jsonBodyObj = new JSONObject();

        try{
            jsonBodyObj.put("dialog_id", dialogID);
            jsonBodyObj.put("message", message);
            jsonBodyObj.put("user_id", userSender.getServerID());
            jsonBodyObj.put("image", imageBase64);
            //jsonBodyObj.put("did", "11_51");
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new MessageJsonRequest(Method.POST, url, jsonBodyObj, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("Authorization", AstrologyApplication.getInstance().getToken());

        return headers;
    }
}
