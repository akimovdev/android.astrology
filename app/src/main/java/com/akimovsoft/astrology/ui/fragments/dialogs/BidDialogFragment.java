package com.akimovsoft.astrology.ui.fragments.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.ui.activities.MessagesActivity;


public class BidDialogFragment extends DialogFragment {

    private TextView mCancelTV;
    private TextView mNotificationTV;
    private Button mOpenBTN;
    private com.akimovsoft.astrology.model.entity.Dialog mDialog;

    public static BidDialogFragment newInstance(com.akimovsoft.astrology.model.entity.Dialog dialog) {
        BidDialogFragment fragment = new BidDialogFragment();
        fragment.mDialog = dialog;
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bid_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNotificationTV = view.findViewById(R.id.tv_notification);
        mCancelTV = view.findViewById(R.id.tv_cancel);
        mOpenBTN = view.findViewById(R.id.btn_open);

        String consultationBid = mDialog.getReceiver().getName() + "\n" +
                getString(R.string.consultation_bid) + " " + mDialog.getService().getName();

        mNotificationTV.setText(consultationBid);

        mOpenBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User currentUser = AstrologyApplication.getInstance().getCurrentUser();

                Intent intent = MessagesActivity.createIntent(getContext(), mDialog, currentUser, mDialog.getReceiver());
                startActivity(intent);
            }
        });

        mCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }
}
