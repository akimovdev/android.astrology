package com.akimovsoft.astrology.model.requests;

import androidx.annotation.Nullable;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.model.entity.User;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class PushNotification extends JsonObjectRequest {

    private PushNotification(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public static PushNotification createPushRequest(String pushToken2, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener){

        //String url = Constants.SERVER_PRODUCTION_API +"messages";
        String url = "https://fcm.googleapis.com/fcm/send";

        JSONObject jsonBodyObj = new JSONObject();
        JSONObject jsonNotificationObj = new JSONObject();

        String pushTokenTest = "eErOcIWe9N4:APA91bHJ8wvMLN74po62p_K-f2T6HUqi29l765OYk54EADtObPm63ClbKgBlBosvVjbhWO-A3u6Nhlh796x-CHA_7RZvyYYkHyvCVNbxMJ7VNlyWpjD51g62WeYxBDkuJgjpDwN39Zpa";

        try{
            jsonNotificationObj.put("body","Server body");
            jsonNotificationObj.put("title","Server title");
            jsonNotificationObj.put("icon","ic_launcher");

            jsonBodyObj.put("to", pushTokenTest);
            jsonBodyObj.put("notification", jsonNotificationObj);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new PushNotification(Method.POST, url, jsonBodyObj, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("Authorization", AstrologyApplication.getInstance().getToken());

        return headers;
    }
}
