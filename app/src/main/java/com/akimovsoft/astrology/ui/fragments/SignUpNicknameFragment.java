package com.akimovsoft.astrology.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;/*
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;*/
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.ImageJsonRequest;
import com.akimovsoft.astrology.model.requests.UserJsonRequest;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnNicknameListener} interface
 * to handle interaction events.
 * Use the {@link SignUpNicknameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUpNicknameFragment extends BaseFragment {

    String TAG = "SIGN_UP_NICKNAME_FRAGMENT";

    private OnNicknameListener mListener;

    private ImageView mPhotoIV;
    private CircleImageView mAvatarCIV;
    private RelativeLayout mPhotoButtonRL;

    private TextInputEditText mNameTIET;
    private TextInputLayout mNameTIL;

    public SignUpNicknameFragment() {
        // Required empty public constructor
    }


    public static SignUpNicknameFragment newInstance() {
        SignUpNicknameFragment fragment = new SignUpNicknameFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up_nickname, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideActionBar();

        mNameTIL = view.findViewById(R.id.til_name);
        mNameTIET = view.findViewById(R.id.input_nickname);

        mPhotoButtonRL = view.findViewById(R.id.rl_photo_button);
        mAvatarCIV = view.findViewById(R.id.civ_avatar);
        mPhotoIV = view.findViewById(R.id.iv_photo);


        mPhotoIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted()) {
                    openGallery();
                }
            }
        });

        Button btnNext = view.findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateInput())
                    return;

                showProgressDialog();
                requestPutNickname(mNameTIET.getText().toString());
            }
        });
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                //ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            openGallery();
        }
    }

    private void requestPutNickname(String nickname) {

        Long userID = AstrologyApplication.getInstance().getCurrentUser().getServerID();
        JSONObject bodyJSONObj = new JSONObject();

        try{
            bodyJSONObj.put(Constants.KEY_JSON_NAME, nickname);
        }catch (JSONException e){
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        UserJsonRequest request = UserJsonRequest.edit(userID, bodyJSONObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    handleResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_DUPLICATE) {
                    Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                }
            }
        });

        queue.add(request);
    }

    /*
    private void openGallery() {

        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(intent, Constants.REQUEST_OPEN_GALLERY);
    }*/

    private void handleUploadAvatarResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);

            if (isSuccess) {

                String urlAvatar = response.getJSONObject(Constants.KEY_JSON_DATA).getString(Constants.KEY_JSON_URL_AVATAR);
                User currentUser = AstrologyApplication.getInstance().getCurrentUser();
                currentUser.setUrlAvatar(urlAvatar);
                currentUser.saveAll();

                mPhotoButtonRL.setVisibility(View.GONE);
                mAvatarCIV.setVisibility(View.VISIBLE);

                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(currentUser.getUrlAvatar(), mAvatarCIV);
                hideProgressDialog();
                //Toast.makeText(getContext(), "SUCCESSFUL", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handleResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);

            if (isSuccess) {

                String nickname = mNameTIET.getText().toString();
                User currentUser = AstrologyApplication.getInstance().getCurrentUser();
                currentUser.setName(nickname);
                currentUser.saveAll();

                hideProgressDialog();
                mListener.onPutNicknameSuccessful();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNicknameListener) {
            mListener = (OnNicknameListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNicknameListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    protected void onOpenGalleryResult(int resultCode, Intent data) {
        super.onOpenGalleryResult(resultCode, data);

        if (resultCode == Activity.RESULT_OK)
            sendImageToServer(data);
    }


    private void sendImageToServer(Intent data) {

        Uri uri = data.getData();

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 75, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            showProgressDialog();
            requestLoadAvatar("Base64, "+encodedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestLoadAvatar(String imageBase64) {

        Long userID = AstrologyApplication.getInstance().getCurrentUser().getServerID();
        JSONObject bodyJSONObj = new JSONObject();

        try{
            bodyJSONObj.put(Constants.KEY_JSON_IMAGE, imageBase64);
        }catch (JSONException e){
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        ImageJsonRequest request = ImageJsonRequest.uploadUserAvatar(userID, imageBase64, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    handleUploadAvatarResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_DUPLICATE) {
                    Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_SHORT).show();
                }
            }
        });

        queue.add(request);
    }

    private void resetError() {
        mNameTIL.setErrorEnabled(false);
    }

    private boolean validateInput() {

        resetError();

        if (TextUtils.isEmpty(mNameTIET.getText())) {
            mNameTIL.setErrorEnabled(true);
            mNameTIL.setError(getString(R.string.alert_empty_field));
            return false;

        }

        return true;
    }

    public interface OnNicknameListener {
        void onPutNicknameSuccessful();
    }
}
