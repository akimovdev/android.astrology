package com.akimovsoft.astrology.ui.fragments.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.adapters.FiltersAdapter;
import com.akimovsoft.astrology.view.DividerItemDecorator;
import java.util.ArrayList;
import java.util.List;

public class FilterDialogFragment extends DialogFragment {

    private FiltersAdapter.OnItemListener listener;

    public FilterDialogFragment() {
        // Required empty public constructor
    }

    public static DialogFragment newInstance(FiltersAdapter.OnItemListener listener) {
        FilterDialogFragment fragment = new FilterDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.listener = listener;
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RelativeLayout mainRL = view.findViewById(R.id.rl_main);
        mainRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });

        RecyclerView rvList = view.findViewById(R.id.rv_list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        rvList.setLayoutManager(linearLayoutManager);

        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        rvList.addItemDecoration(dividerItemDecoration);

        List<FilterEntity> listEntity = new ArrayList<>();
        listEntity.add(new FilterEntity(getString(R.string.increase_price), FilterEntity.INCREASE_PRICE));
        listEntity.add(new FilterEntity(getString(R.string.descending_price), FilterEntity.DESCENDING_PRICE));
        listEntity.add(new FilterEntity(getString(R.string.high_rating), FilterEntity.HIGH_RATING));
        listEntity.add(new FilterEntity(getString(R.string.high_experience), FilterEntity.HIGH_EXPERIENCE));
        listEntity.add(new FilterEntity(getString(R.string.speed), FilterEntity.SPEED));
        listEntity.add(new FilterEntity(getString(R.string.disciple), FilterEntity.DISCIPLE));

        rvList.setAdapter(FiltersAdapter.createAdapter(listEntity, listener));
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }


    public class FilterEntity {

        public final static String INCREASE_PRICE = "price";
        public final static String DESCENDING_PRICE = "-price";
        public final static String HIGH_RATING = "rating";
        public final static String HIGH_EXPERIENCE = "experience";
        public final static String SPEED = "speed";
        public final static String DISCIPLE = "disciple";

        private int id;
        private String name;
        private String sort;

        public FilterEntity(String name, String sort) {
            this.name = name;
            this.sort = sort;
        }

        public String getName() {
            return name;
        }

        public String getSort() {
            return sort;
        }
    }
}