package com.akimovsoft.astrology.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.model.requests.UserJsonRequest;
import com.akimovsoft.astrology.ui.activities.MainActivity;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class AstroFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();
            //Log.d(TAG, "Message Notification Body: " + body + " title: "+title);

            //String dialogIdStr = remoteMessage.getData().get("dialog_id");
            //long dialog_id = Long.valueOf(dialogIdStr);

            sendNotification(title, body);
            sendBroadcast(remoteMessage.getData());

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        sendRegistrationToServer(token);
    }

    private void scheduleJob() {
        // [START dispatch_job]
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(FCMWorker.class)
                .build();
        WorkManager.getInstance().beginWith(work).enqueue();
        // [END dispatch_job]
    }

    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    private void sendRegistrationToServer(String token) {
        SharedPreferences sharedPref = getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.PREF_PUSH_TOKEN, token);
        editor.apply();

        if (sharedPref.contains(Constants.PREF_CURRENT_USER_SERVER_ID)) {
            long currentUserServerID = sharedPref.getLong(Constants.PREF_CURRENT_USER_SERVER_ID, -1);

            JSONObject bodyJSONObj = new JSONObject();

            try{
                bodyJSONObj.put(Constants.KEY_JSON_PUSH_TOKEN, token);
            }catch (JSONException e){
                e.printStackTrace();
            }

            RequestQueue queue = Volley.newRequestQueue(this);

            UserJsonRequest request = UserJsonRequest.edit(currentUserServerID, bodyJSONObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_DUPLICATE) {
                        //Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            queue.add(request);
        }
    }

    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_message)
                        .setContentTitle(title)
                        //.setContentTitle(getString(R.string.fcm_message))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendBroadcast(Map<String, String> data) {
        Log.d("sender", "Broadcasting message");

        long dialogID = Long.valueOf(data.get("dialog_id"));
        String avatarURL = data.get("url_avatar");
        String userName = data.get("name");
        String categoryName = data.get("category_name");
        String did = data.get("did");
        int state = Integer.valueOf(data.get("status"));


        Intent intent = new Intent(Constants.BROADCAST_BID);
        intent.putExtra(Constants.EXTRA_USER_NAME, userName);
        intent.putExtra(Constants.EXTRA_SERVICE_NAME, categoryName);
        intent.putExtra(Constants.EXTRA_IMAGE_URL, avatarURL);
        intent.putExtra(Constants.EXTRA_DIALOG_SERVER_ID, dialogID);
        intent.putExtra(Constants.EXTRA_DIALOG_DID, did);
        intent.putExtra(Constants.EXTRA_DIALOG_STATUS, state);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
