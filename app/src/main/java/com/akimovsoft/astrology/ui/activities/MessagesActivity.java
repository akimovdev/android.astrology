package com.akimovsoft.astrology.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.ui.fragments.MessagesFragment;

public class MessagesActivity extends BaseActivity {

    private TextView mToolBarTV;

    static User userSenderTemp;
    static User userReceiverTemp;
    static Dialog dialogTemp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        User userSender = userSenderTemp;
        User userReceiver = userReceiverTemp;
        Dialog dialog = dialogTemp;

        userSenderTemp = null;
        userReceiverTemp = null;
        dialogTemp = null;

        mToolBarTV = findViewById(R.id.toolbar_title);
        mToolBarTV.setText(getString(R.string.list_of_services));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        /*
        if (dialogID == -1 || expertID == -1)
            throw new Error("Extra has been lost !!!");*/

        replaceFragment(MessagesFragment.newInstance(dialog, userSender, userReceiver));
    }

    public void setCustomTitle(String title) {
        mToolBarTV.setText(title);
    }

    public static Intent createIntent(Context context, Dialog dialog, User userSender, User userReceiver) {

        MessagesActivity.userSenderTemp = userSender;
        MessagesActivity.userReceiverTemp = userReceiver;
        MessagesActivity.dialogTemp= dialog;

        Intent intent = new Intent(context, MessagesActivity.class);
        //intent.putExtra(Constants.EXTRA_DIALOG_ID, dialogID);
        //intent.putExtra(Constants.EXTRA_EXPERT_SERVER_ID, expertID);

        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }
}
