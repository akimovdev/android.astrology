package com.akimovsoft.astrology.ui.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.activities.AuthActivity;
import com.akimovsoft.astrology.ui.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {


    protected void showActionBar() {
        if (getActivity() instanceof AuthActivity) {
            ActionBar actionBar =  ((AuthActivity)getActivity()).getSupportActionBar();
            if (actionBar != null)
                actionBar.show();
        }
    }

    protected void hideActionBar() {
        if (getActivity() instanceof AuthActivity) {
            ActionBar actionBar =  ((AuthActivity)getActivity()).getSupportActionBar();
            if (actionBar != null)
                actionBar.hide();
        }
    }

    protected void showProgressDialog() {
        ((BaseActivity)getActivity()).showProgressDialog();
    }

    protected void hideProgressDialog() {
        ((BaseActivity)getActivity()).hideProgressDialog();
    }

    protected void openGallery() {

        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(intent, Constants.REQUEST_OPEN_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


            if (requestCode == Constants.REQUEST_OPEN_GALLERY)
                onOpenGalleryResult(resultCode, data);

    }


    protected void onOpenGalleryResult(int resultCode, Intent data) {

    }
}
