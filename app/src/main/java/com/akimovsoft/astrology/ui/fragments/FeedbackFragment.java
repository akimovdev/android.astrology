package com.akimovsoft.astrology.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.requests.DialogJsonRequest;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import org.json.JSONException;
import org.json.JSONObject;


public class FeedbackFragment extends BaseFragment {
    private String TAG = "MESSAGES_FRAGMENT";
    private RequestQueue mQueue;

    private final static String ARG_EXPERT_ID = "arg_expert_id";
    private final static String ARG_DIALOG_ID = "arg_dialog_id";
    private final static String ARG_USER_ID = "arg_user_id";

    private long mExpertID;
    private long mDialogID;
    private long mUserID;
    private String mCommentary;
    private float mRating;

    private TextInputLayout mCommentTIL;
    private TextInputEditText mCommentTIET;
    //private EditText commentET;
    private RatingBar ratingBar;

    public FeedbackFragment() {
        // Required empty public constructor
    }

    public static FeedbackFragment newInstance(long expertID, long dialogID, long userID) {
        FeedbackFragment fragment = new FeedbackFragment();
        Bundle args = new Bundle();

        args.putLong(ARG_EXPERT_ID, expertID);
        args.putLong(ARG_DIALOG_ID, dialogID);
        args.putLong(ARG_USER_ID, userID);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDialogID = getArguments().getLong(ARG_DIALOG_ID);
            mExpertID = getArguments().getLong(ARG_EXPERT_ID);
            mUserID = getArguments().getLong(ARG_USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button sendBTN = view.findViewById(R.id.btn_close);
        ratingBar = view.findViewById(R.id.ratingBar);
        mCommentTIET = view.findViewById(R.id.tiet_comment);
        mCommentTIL = view.findViewById(R.id.til_comment);

        sendBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateInput())
                    return;

                showProgressDialog();
                onSendClick();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mQueue != null) {
            mQueue.cancelAll(TAG);
        }
    }

    private void onSendClick() {
        mCommentary = mCommentTIET.getText().toString();
        mRating = ratingBar.getRating();

        requestSendFeedback();
    }

    private void requestSendFeedback() {

        mQueue = Volley.newRequestQueue(getContext());

        JSONObject jsonBodyObj = new JSONObject();

        try{
            jsonBodyObj.put("expert_id", mExpertID);
            jsonBodyObj.put("commentary", mCommentary);
            jsonBodyObj.put("rating", mRating);
            jsonBodyObj.put("dialog_id", mDialogID);
            jsonBodyObj.put("user_id", mUserID);
        }catch (JSONException e){
            e.printStackTrace();
        }

        DialogJsonRequest dialogJsonRequest = DialogJsonRequest.closeDialog(mDialogID, jsonBodyObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                closeResultOkActivity();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                hideProgressDialog();
                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                Log.d("FEEDBACK_REQUEST", new String(error.networkResponse.data));
            }
        });

        dialogJsonRequest.setTag(TAG);
        mQueue.add(dialogJsonRequest);
    }

    private void closeResultOkActivity() {
        Intent data = new Intent();
        getActivity().setResult(Activity.RESULT_OK, data);
        getActivity().finish();
    }

    private void resetError() {
        mCommentTIL.setErrorEnabled(false);
    }

    private boolean validateInput() {

        resetError();

        if (TextUtils.isEmpty(mCommentTIET.getText())) {
            mCommentTIL.setErrorEnabled(true);
            mCommentTIL.setError(getString(R.string.alert_empty_field));
            return false;
        } else if (ratingBar.getRating() == 0) {
            Toast.makeText(getContext(), getString(R.string.empty_rating), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
