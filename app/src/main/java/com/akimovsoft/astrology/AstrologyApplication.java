package com.akimovsoft.astrology;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.akimovsoft.astrology.model.entity.City;
import com.akimovsoft.astrology.model.entity.User;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class AstrologyApplication extends Application {

    private String mToken;
    private String mPushToken;
    private User mCurrentUser;
    private SharedPreferences mSharedPref;


    @Override
    public void onCreate() {
        super.onCreate();

        synchronized (AstrologyApplication.class) {
            if (BuildConfig.DEBUG) {
                if (instance != null)
                    throw new RuntimeException("Something strange: there is another application newMoreLessInstance.");
            }
            instance = this;

            AstrologyApplication.class.notifyAll();
        }

        mSharedPref = getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);

        Configuration dbConfiguration = new Configuration.Builder(this)
                .setDatabaseName("Astrology.db")
                .addModelClass(User.class)
                .addModelClass(City.class)
                .create();

        ActiveAndroid.initialize(dbConfiguration);

        initValues();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
    }

    private volatile static AstrologyApplication instance;

    @SuppressWarnings({"ConstantConditions", "unchecked"})
    public static AstrologyApplication getInstance() {
        AstrologyApplication application = instance;
        if (application == null) {
            synchronized (AstrologyApplication.class) {
                if (instance == null) {
                    if (BuildConfig.DEBUG) {
                        if (Thread.currentThread() == Looper.getMainLooper().getThread())
                            throw new UnsupportedOperationException(
                                    "Current application's newMoreLessInstance has not been initialized yet (wait for onCreate, please).");
                    }

                    try {
                        do {
                            AstrologyApplication.class.wait();
                        } while ((application = instance) == null);
                    } catch (InterruptedException e) {
                        /* Nothing to do */
                    }
                }
            }
        }

        return application;
    }

    private void initValues() {

        if (mCurrentUser == null && mSharedPref.contains(Constants.PREF_CURRENT_USER_ID)) {
            Long id = mSharedPref.getLong(Constants.PREF_CURRENT_USER_ID, -1);
            mCurrentUser = User.load( User.class,  id);
        }

        if (mSharedPref.contains(Constants.PREF_CURRENT_TOKEN)) {
            mToken = mSharedPref.getString(Constants.PREF_CURRENT_TOKEN, null);
        }

        if (mSharedPref.contains(Constants.PREF_PUSH_TOKEN)) {
            mPushToken = mSharedPref.getString(Constants.PREF_PUSH_TOKEN, null);
        }
    }

    public void setToken(String token) {
        this.mToken = token;

        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(Constants.PREF_CURRENT_TOKEN, mToken);
        editor.apply();
    }

    public String getToken() {
        return mToken;
    }

    public void setPushToken(String pushToken) {
        this.mPushToken = pushToken;

        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(Constants.PREF_PUSH_TOKEN, mPushToken);
        editor.apply();
    }

    public String getPushToken() {
        if (mPushToken == null)
            mPushToken = mSharedPref.getString(Constants.PREF_PUSH_TOKEN, null);

        return mPushToken;
    }

    public void setCurrentUser(User currentUser) {
        mCurrentUser = currentUser;

        Long id = mCurrentUser.saveAll();

        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putLong(Constants.PREF_CURRENT_USER_ID, id);
        editor.putLong(Constants.PREF_CURRENT_USER_SERVER_ID, mCurrentUser.getServerID());
        editor.apply();
    }

    public User getCurrentUser() {
        /*
        if (mCurrentUser == null && mSharedPref.contains(Constants.PREF_CURRENT_USER_ID)) {
            Long id = mSharedPref.getLong(Constants.PREF_CURRENT_USER_ID, -1);
            mCurrentUser = User.load( User.class,  id);
        }*/

        return mCurrentUser;
    }

    public boolean isCurrentUser(User user) {

        long serverID = mCurrentUser.getServerID();
        long serverID1 = user.getServerID();

        return mCurrentUser != null && mCurrentUser.getServerID() == user.getServerID();
    }
}
