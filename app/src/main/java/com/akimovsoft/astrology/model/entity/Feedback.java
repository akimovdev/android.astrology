package com.akimovsoft.astrology.model.entity;

import com.akimovsoft.astrology.model.Expert;
import com.google.gson.annotations.Expose;

public class Feedback {

    private String commentary;
    private User user;

    @Expose
    private Expert expert;

    public String getCommentary() {
        return commentary;
    }

    public User getUser() {
        return user;
    }

    public Expert getExpert() {
        return expert;
    }
}
