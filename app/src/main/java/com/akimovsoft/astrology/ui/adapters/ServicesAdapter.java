package com.akimovsoft.astrology.ui.adapters;

/*
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;*/
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Service;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    private final OnItemListener listener;
    private final List<Service> items;

    public static ServicesAdapter createAdapter(List<Service> items, OnItemListener listener) {

        return new ServicesAdapter(items, listener);
    }

    private ServicesAdapter(List<Service> items, OnItemListener listener) {
        this.items = items;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dash_service, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.item = items.get(position);

        holder.tvName.setText(holder.item.getName());

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(Constants.SERVER_PRODUCTION+holder.item.getUrlIcon(), holder.ivIcon);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onItemInteraction(holder.item);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemListener {
        void onItemInteraction(Service game);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView tvName;
        public final ImageView ivIcon;

        public Service item;

        public ViewHolder(View view) {
            super(view);

            mView = view;
            ivIcon = view.findViewById(R.id.iv_icon);
            tvName = view.findViewById(R.id.tv_name);
        }
    }
}
