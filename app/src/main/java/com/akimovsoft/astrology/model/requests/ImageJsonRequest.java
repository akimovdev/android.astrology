package com.akimovsoft.astrology.model.requests;

import androidx.annotation.Nullable;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class ImageJsonRequest extends JsonObjectRequest {


    private ImageJsonRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public static ImageJsonRequest uploadUserAvatar(long userID, String imageBase64, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {

        JSONObject jsonBodyObj = new JSONObject();
        String url = Constants.SERVER_PRODUCTION_API +"images/uploadUserAvatar/"+userID;

        try{
            jsonBodyObj.put(Constants.KEY_JSON_IMAGE, imageBase64);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new ImageJsonRequest(Request.Method.POST, url, jsonBodyObj, listener, errorListener);
    }

    /*
    public static ImageJsonRequest uploadMessageImage(long userID, String imageBase64, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {

        JSONObject jsonBodyObj = new JSONObject();
        String url = Constants.SERVER_PRODUCTION_API +"images/uploadUserAvatar/"+userID;

        try{
            jsonBodyObj.put(Constants.KEY_JSON_IMAGE, imageBase64);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new ImageJsonRequest(Request.Method.POST, url, jsonBodyObj, listener, errorListener);
    }*/

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");

        if (AstrologyApplication.getInstance().getToken() != null)
            headers.put("Authorization", AstrologyApplication.getInstance().getToken());

        return headers;
    }
}
