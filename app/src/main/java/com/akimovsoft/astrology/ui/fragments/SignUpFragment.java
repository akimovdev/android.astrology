package com.akimovsoft.astrology.ui.fragments;

import android.content.Context;
import android.os.Bundle;/*
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;*/

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.UserJsonRequest;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpFragment extends BaseFragment {

    private OnSignUpListener mListener;
    private User mUser;

    private TextInputEditText mEmailTIET;
    private TextInputEditText mPasswordTIET;
    private TextInputEditText mRepasswordTIET;

    private TextInputLayout mEmailTIL;
    private TextInputLayout mPasswordTIL;
    private TextInputLayout mRepasswordTIL;

    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mUser = new User();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showActionBar();

        mEmailTIL = view.findViewById(R.id.til_email);
        mPasswordTIL = view.findViewById(R.id.til_password);
        mRepasswordTIL = view.findViewById(R.id.til_repassword);

        mEmailTIET = view.findViewById(R.id.tiet_email);
        mEmailTIET.setFilters(new InputFilter[] { filter });

        mPasswordTIET = view.findViewById(R.id.tiet_password);
        mRepasswordTIET = view.findViewById(R.id.tiet_repassword);

        Button registerBTN = view.findViewById(R.id.btn_next);
        registerBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateInput())
                    return;

                showProgressDialog();

                String email = mEmailTIET.getText().toString();
                String password = mPasswordTIET.getText().toString();

                if (email.isEmpty()) {
                    Toast.makeText(getContext(), "Error empty email", Toast.LENGTH_LONG).show();
                    return;
                } else if(password.isEmpty()) {
                    Toast.makeText(getContext(), "Error empty password", Toast.LENGTH_LONG).show();
                    return;
                }

                mUser.setEmail(email);

                registerRequest(email, password);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignInFragment.OnSignInListener) {
            mListener = (OnSignUpListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSignUpListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void registerRequest(String email, String password) {

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        UserJsonRequest requester = UserJsonRequest.register(email, password, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    onRegisterResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();

                if (error == null || error.networkResponse == null) {
                    Toast.makeText(getActivity(), getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_DUPLICATE) {
                    Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }
            }
        });
        queue.add(requester);
    }

    private void onRegisterResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);

            if (isSuccess) {
                onRegisterResponseSuccessful(response);
            }
        }
    }

    private void onRegisterResponseSuccessful(JSONObject response) throws JSONException {

        String token = response
                .getJSONObject(Constants.KEY_JSON_DATA)
                .getString(Constants.KEY_JSON_TOKEN);

        long serverID = response
                .getJSONObject(Constants.KEY_JSON_DATA)
                .getLong(Constants.KEY_JSON_SERVER_ID);

        mUser.setServerID(serverID);



        AstrologyApplication.getInstance().setCurrentUser(mUser);
        AstrologyApplication.getInstance().setToken(token);

        hideProgressDialog();

        mListener.onSignUpSuccessful();
    }

    private void resetError() {
        mEmailTIL.setErrorEnabled(false);
        mPasswordTIL.setErrorEnabled(false);
        mRepasswordTIL.setErrorEnabled(false);
    }

    private boolean validateInput() {

        resetError();

        if (TextUtils.isEmpty(mEmailTIET.getText())) {
            mEmailTIL.setErrorEnabled(true);
            mEmailTIL.setError(getString(R.string.alert_empty_field));
            return false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmailTIET.getText()).matches()) {
            mEmailTIL.setErrorEnabled(true);
            mEmailTIL.setError(getString(R.string.alert_unappropriate_format));
            return false;

        } else if (TextUtils.isEmpty(mPasswordTIET.getText())) {
            mPasswordTIL.setErrorEnabled(true);
            mPasswordTIL.setError(getString(R.string.alert_empty_field));
            return false;
        } else if (TextUtils.isEmpty(mRepasswordTIET.getText())) {
            mRepasswordTIL.setErrorEnabled(true);
            mRepasswordTIL.setError(getString(R.string.alert_empty_field));
            return false;
        } else if (!mPasswordTIET.getText().toString().equals(mRepasswordTIET.getText().toString())) {

            Toast.makeText(getContext(), getString(R.string.alert_passwords_not_equal), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);
            for (int i = start; i < end; i++) {
                char c = source.charAt(i);
                if (isCharAllowed(c)) // put your condition here
                    sb.append(c);
                else
                    keepOriginal = false;
            }
            if (keepOriginal)
                return null;
            else {
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(sb);
                    TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                    return sp;
                } else {
                    return sb;
                }
            }
        }

        private boolean isCharAllowed(char c) {
            return c != ' ';
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public interface OnSignUpListener {
        void onSignUpSuccessful();
    }
}