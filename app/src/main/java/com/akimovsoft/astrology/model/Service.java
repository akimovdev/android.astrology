package com.akimovsoft.astrology.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Service {

    private long id;

    @Expose
    @SerializedName("url_icon")
    private String urlIcon;

    private String name;

    public Service(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrlIcon() {
        return urlIcon;
    }
}
