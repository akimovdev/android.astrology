package com.akimovsoft.astrology.ui.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DialogsAdapter extends RecyclerView.Adapter<DialogsAdapter.ViewHolder> {

    private List<Dialog> dialogList = new ArrayList<>();
    private OnItemListener listener;
    private Drawable activeDrawable;
    private Drawable nonactiveDrawable;


    public DialogsAdapter(List<Dialog> messageList, OnItemListener listener){
        this.dialogList.addAll(messageList);
        this.listener = listener;
        this.activeDrawable = activeDrawable;
        this.nonactiveDrawable = nonactiveDrawable;
    }

    public static DialogsAdapter createAdapter(List<Dialog> items, OnItemListener listener) {
        return new DialogsAdapter(items, listener);
    }

    @NonNull
    @Override
    public DialogsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_dialog, viewGroup, false);
        return new DialogsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
        viewHolder.mItem = dialogList.get(position);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(viewHolder.mView.getContext()).build();
        ImageLoader.getInstance().init(config);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(viewHolder.mItem.getReceiver().getUrlAvatar(), viewHolder.civAvatar);

        viewHolder.nameTV.setText(viewHolder.mItem.getReceiver().getName());
        viewHolder.serviceTV.setText(viewHolder.mItem.getService().getName());
        viewHolder.experienceTV.setText("Стаж "+viewHolder.mItem.getExperience()+" лет");

        if (viewHolder.mItem.getState() == Dialog.STATE_ACTIVE) {
            viewHolder.openBTN.setText("Продолжить диалог");
            viewHolder.openBTN.setBackground(ContextCompat.getDrawable(viewHolder.mView.getContext(), R.drawable.bg_accent_add_rcon_5dp));
        } else if (viewHolder.mItem.getState() == Dialog.STATE_CLOSE) {
            viewHolder.openBTN.setText("История чата");
            viewHolder.openBTN.setBackground(ContextCompat.getDrawable(viewHolder.mView.getContext(), R.drawable.bg_primary_rcorn_5dp));
        } else {
            viewHolder.openBTN.setText("Войти в чат");
            viewHolder.openBTN.setBackground(ContextCompat.getDrawable(viewHolder.mView.getContext(), R.drawable.bg_primary_rcorn_5dp));
        }

        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onItemInteraction(viewHolder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dialogList.size();
    }

    public List<Dialog> getItems() {
        return dialogList;
    }

    public interface OnItemListener {
        void onItemInteraction(Dialog dialog);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public Dialog mItem;
        public final TextView nameTV;
        public final TextView serviceTV;
        public final TextView experienceTV;
        public final Button openBTN;
        public final CircleImageView civAvatar;

        /*
        public final CircleImageView civAvatar;
        public final TextView tvSymbol;
        public final TextView mContentView;
        public final ProgressBar progressBar;
        */

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameTV = view.findViewById(R.id.tv_name);
            serviceTV = view.findViewById(R.id.tv_service);
            experienceTV = view.findViewById(R.id.tv_experience);
            civAvatar = view.findViewById(R.id.civ_avatar);
            openBTN = view.findViewById(R.id.btn_open);

            /*
            tvSymbol = (TextView) view.findViewById(R.id.tvSymbol);
            civAvatar = (CircleImageView) view.findViewById(R.id.civAvatar);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);*/
        }
    }
}
