package com.akimovsoft.astrology.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Gravity;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v7.app.AppCompatActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.fragments.dialogs.ProgressPopupWindow;


public class BaseActivity extends AppCompatActivity {

    private Vibrator mVibrator;
    private ProgressPopupWindow popupWindow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //setTheme(SettingsManager.getInstance(getApplicationContext()).getCurrentThemeResource());

        super.onCreate(savedInstanceState);

        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commitNow();
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.container, fragment);

        if (addToBackStack)
            transaction.addToBackStack( fragment.getClass().getName() );

        transaction.commit();
    }

    public void addFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.add(R.id.container, fragment);

        transaction.addToBackStack( fragment.getClass().getName() );

        transaction.commit();
        //replaceFragment(fragment);
    }

    public void showProgressDialog() {
        popupWindow = ProgressPopupWindow.create(this);
        popupWindow.showAtLocation(Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(false);
    }

    public void hideProgressDialog() {
        if (popupWindow == null)
            return;

        popupWindow.dismiss();
        popupWindow = null;
    }

    //public void vibrate(long milliseconds) {
        //mVibrator.vibrate(milliseconds);
    //}
}
