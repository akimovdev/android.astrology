package com.akimovsoft.astrology.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.UserJsonRequest;
import com.akimovsoft.astrology.ui.activities.AuthActivity;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;


public class PersonalDataFragment extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int mHourOfDay = -1;
    private int mMinute = -1;
    private int mDay = -1;
    private int mMonth = -1;
    private int mYear = -1;

    private OnPersonalDataListener mListener;

    public PersonalDataFragment() {
        // Required empty public constructor
    }


    public static PersonalDataFragment newInstance() {
        PersonalDataFragment fragment = new PersonalDataFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showActionBar();

        AuthActivity authActivity = ((AuthActivity)getActivity());

        if (authActivity != null) {
            authActivity.isToolBarTitleVisible(true);
            authActivity.setToolBarTitle(getString(R.string.title_personal_data));
            authActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_personal_data, container, false);
    }

    private TextInputEditText mInputDate;
    private TextInputEditText mInputTime;
    private TextInputEditText mInputCityBirth;
    private TextInputEditText mInputCityLive;
    private TextInputLayout mCityBirthTIL;
    private TextInputLayout mCityLiveTIL;


    private OnPickerListener mPickerListener = new OnPickerListener() {
        @Override
        public void onTimeSet(int hourOfDay, int minute) {
            mHourOfDay = hourOfDay;
            mMinute = minute;

            fillTimeField();
        }

        @Override
        public void onDateSet(int year, int month, int day) {
            mYear = year;
            mMonth = month;
            mDay = day;

            fillDateField();
        }
    };

    protected void fillTimeField() {

        String hour;
        String minute;

        if (mHourOfDay < 10) {
            hour = "0"+mHourOfDay;
        } else {
            hour = String.valueOf(mHourOfDay);
        }

        if (mMinute < 10) {
            minute = "0"+mMinute;
        } else {
            minute = String.valueOf(mMinute);
        }

        String time = hour + " : " + minute;
        mInputTime.setText(time);
    }


    protected void fillDateField() {

        String day;
        String month;

        if (mDay < 10) {
            day = "0"+mDay;
        } else {
            day = String.valueOf(mDay);
        }

        if (mMonth < 10) {
            month = "0"+mMonth;
        } else {
            month = String.valueOf(mMonth);
        }

        String date = day + "." + month + "." + mYear;
        mInputDate.setText(date);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mInputDate = view.findViewById(R.id.input_date);
        mInputTime = view.findViewById(R.id.input_time);
        mInputCityBirth = view.findViewById(R.id.input_city_birth);
        mInputCityLive = view.findViewById(R.id.input_city_live);

        mCityBirthTIL = view.findViewById(R.id.til_city_birth);
        mCityLiveTIL = view.findViewById(R.id.til_city_live);

        View dateClickZone = view.findViewById(R.id.date_click_zone);
        dateClickZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment(mPickerListener);
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        View timeClickZone = view.findViewById(R.id.time_click_zone);
        timeClickZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment newFragment = new TimePickerFragment(mPickerListener);
                newFragment.show(getFragmentManager(), "timePicker");
            }
        });


        InputFilter filterTxtLive = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i))) {
                        return mInputCityLive.getText().toString();
                    }
                }
                return null;
            }
        };

        InputFilter filterTxtBirth = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i))) {
                        return mInputCityBirth.getText().toString();
                    }
                }
                return null;
            }
        };

        mInputCityLive.setFilters(new InputFilter[]{filterTxtLive});
        mInputCityBirth.setFilters(new InputFilter[]{filterTxtBirth});

        Button doneBTN = view.findViewById(R.id.btn_done);
        doneBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateInput())
                    return;

                showProgressDialog();

                String cityBirth = mInputCityBirth.getText().toString();
                String cityLive = mInputCityLive.getText().toString();

                requestPutPersonalData(getBirthTimestamp(), cityBirth, cityLive);
            }
        });
    }

    private long getBirthTimestamp() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, mYear);
        cal.set(Calendar.MONTH, mMonth);
        cal.set(Calendar.DAY_OF_MONTH, mDay);
        cal.set(Calendar.HOUR_OF_DAY, mHourOfDay);
        cal.set(Calendar.MINUTE, mMinute);

        return cal.getTimeInMillis()/1000;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPersonalDataListener) {
            mListener = (OnPersonalDataListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void requestPutPersonalData(long birthTimestamp, String cityBirth, String cityLive) {

        //Long userID = AstrologyApplication.getInstance().getCurrentUser();//.getServerID();

        User user = AstrologyApplication.getInstance().getCurrentUser();
        user.setBirthTimestamp(birthTimestamp);
        user.setCityBirth(cityBirth);
        user.setCityLive(cityLive);

        JSONObject bodyJSONObj = new JSONObject();

        try{
            bodyJSONObj.put(Constants.KEY_JSON_BIRTH_TIME, birthTimestamp);
            bodyJSONObj.put(Constants.KEY_JSON_BIRTH_CITY, cityBirth);
            bodyJSONObj.put(Constants.KEY_JSON_LIVE_CITY, cityLive);
        }catch (JSONException e){
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        UserJsonRequest request = UserJsonRequest.edit(user.getServerID(), bodyJSONObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    handleResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_DUPLICATE) {
                    Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                }
            }
        });

        queue.add(request);
    }

    private void handleResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);

            if (isSuccess) {
                User user = AstrologyApplication.getInstance().getCurrentUser();
                user.saveAll();

                hideProgressDialog();
                mListener.onPersonalDataSuccessful();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void resetError() {
        mCityBirthTIL.setErrorEnabled(false);
        mCityLiveTIL.setErrorEnabled(false);
    }

    private boolean validateInput() {

        resetError();

        if (TextUtils.isEmpty(mInputCityBirth.getText())) {
            mCityBirthTIL.setErrorEnabled(true);
            mCityBirthTIL.setError(getString(R.string.alert_empty_field));
            return false;
        } else if (TextUtils.isEmpty(mInputCityLive.getText())) {
            mCityLiveTIL.setErrorEnabled(true);
            mCityLiveTIL.setError(getString(R.string.alert_empty_field));
            return false;
        } else if (mHourOfDay == -1 || mMinute == -1) {
            Toast.makeText(getContext(), getString(R.string.alert_empty_time_of_birth), Toast.LENGTH_SHORT).show();
            return false;
        } else if (mDay == -1 || mMonth == -1 || mYear == -1) {
            Toast.makeText(getContext(), getString(R.string.alert_empty_date_of_birth), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public interface OnPersonalDataListener {
        void onPersonalDataSuccessful();
    }

    public interface OnPickerListener {
        void onTimeSet(int hourOfDay, int minute);
        void onDateSet(int year, int month, int day);
    }

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        private OnPickerListener mListener;

        private int mHourOfDay;
        private int mMinute;

        TimePickerFragment(OnPickerListener listener) {
            super();
            mListener = listener;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute, true);
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mListener.onTimeSet(hourOfDay, minute);
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private OnPickerListener mListener;

        DatePickerFragment(OnPickerListener listener) {
            super();
            mListener = listener;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            mListener.onDateSet(year, month, day);
        }
    }
}
