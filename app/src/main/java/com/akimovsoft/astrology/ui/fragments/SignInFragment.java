package com.akimovsoft.astrology.ui.fragments;

import android.content.Context;
import android.os.Bundle;/*
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;*/
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.UserJsonRequest;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;


public class SignInFragment extends BaseFragment {

    private OnSignInListener mListener;

    private TextInputLayout mEmailTIL;
    private TextInputLayout mPasswordTIL;

    private TextInputEditText mEmailTIET;
    private TextInputEditText mPasswordTIET;

    public SignInFragment() {
        // Required empty public constructor
    }


    public static SignInFragment newInstance(/*String param1, String param2*/) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showActionBar();

        mEmailTIET = view.findViewById(R.id.tiet_email);
        mPasswordTIET = view.findViewById(R.id.tiet_password);

        mEmailTIL = view.findViewById(R.id.til_email);
        mPasswordTIL = view.findViewById(R.id.til_password);

        Button btnSignIn = view.findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateInput())
                    return;

                showProgressDialog();
                authRequest(mEmailTIET.getEditableText().toString(), mPasswordTIET.getEditableText().toString());
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSignInListener) {
            mListener = (OnSignInListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSignInListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void authRequest(String email, String password) {

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        UserJsonRequest request = UserJsonRequest.auth(email, password, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    handleResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                if ( error.networkResponse != null
                        && error.networkResponse.statusCode == Constants.CODE_RESPONSE_UNAUTHORISED ) {
                    Toast.makeText(getActivity(), getString(R.string.invalid_username_or_password), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.auth_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

        queue.add(request);
    }

    private void handleResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);

            if (isSuccess) {
                handleSignInSuccessful(response);
            }
        }
    }

    private void handleSignInSuccessful(JSONObject response) throws JSONException {

        JSONObject userJSON = response
                .getJSONObject(Constants.KEY_JSON_DATA)
                .getJSONObject(Constants.KEY_JSON_USER);

        String token = response
                .getJSONObject(Constants.KEY_JSON_DATA)
                .getString(Constants.KEY_JSON_TOKEN);

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);

        Gson gson = builder.create();

        User user = gson.fromJson(userJSON.toString(), User.class);

        AstrologyApplication.getInstance().setCurrentUser(user);
        AstrologyApplication.getInstance().setToken(token);

        hideProgressDialog();
        mListener.onSignInSuccessful();
    }

    private void resetError() {
        mEmailTIL.setErrorEnabled(false);;
        mPasswordTIL.setErrorEnabled(false);
    }

    private boolean validateInput() {

        resetError();

        if (TextUtils.isEmpty(mEmailTIET.getText())) {
            mEmailTIL.setErrorEnabled(true);
            mEmailTIL.setError(getString(R.string.alert_empty_field));
            return false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmailTIET.getText()).matches()) {
            mEmailTIL.setErrorEnabled(true);
            mEmailTIL.setError(getString(R.string.alert_unappropriate_format));
            return false;

        } else if (TextUtils.isEmpty(mPasswordTIET.getText())) {
            mPasswordTIL.setErrorEnabled(true);
            mPasswordTIL.setError(getString(R.string.alert_empty_field));
            return false;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public interface OnSignInListener {
        void onSignInSuccessful();
    }
}
