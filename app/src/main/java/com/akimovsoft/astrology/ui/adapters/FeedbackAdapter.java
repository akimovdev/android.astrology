package com.akimovsoft.astrology.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Feedback;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {

    private OnItemListener listener;
    private final List<Feedback> items;


    public static FeedbackAdapter createAdapter(List<Feedback> items/*, OnItemListener listener*/) {

        return new FeedbackAdapter (items/*, listener*/);
    }

    private FeedbackAdapter (List<Feedback> items/*, OnItemListener listener*/) {
        this.items = items;
        //this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_feedback, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        viewHolder.item = items.get(i);

        String name = viewHolder.item.getUser().getName();

        viewHolder.tvCommentary.setText(viewHolder.item.getCommentary());
        viewHolder.tvName.setText(name);
        viewHolder.tvSevice.setText(viewHolder.item.getExpert().getService().getName());
        viewHolder.tvSymbol.setText(String.valueOf(name.charAt(0)));

        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onItemInteraction(viewHolder.item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemListener {
        void onItemInteraction(Feedback feedback);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView tvCommentary;
        public final TextView tvSevice;
        public final TextView tvName;
        public final TextView tvSymbol;
        public final CircleImageView civAvatar;

        public Feedback item;

        public ViewHolder(View view) {
            super(view);

            mView = view;
            tvName = view.findViewById(R.id.tv_name);
            tvCommentary = view.findViewById(R.id.tv_commentary);
            tvSevice = view.findViewById(R.id.tv_service);
            tvSymbol = view.findViewById(R.id.tv_symbol);
            civAvatar = view.findViewById(R.id.civ_avatar);
            /*
            ratingBar = view.findViewById(R.id.ratingBar);
            tvExperience = view.findViewById(R.id.tv_experience);
            tvService = view.findViewById(R.id.tv_service);
            tvPrice = view.findViewById(R.id.tv_price);*/
        }
    }
}

