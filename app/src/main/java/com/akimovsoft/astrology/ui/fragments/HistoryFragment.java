package com.akimovsoft.astrology.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.DialogRequest;
import com.akimovsoft.astrology.ui.activities.MainActivity;
import com.akimovsoft.astrology.ui.activities.MessagesActivity;
import com.akimovsoft.astrology.ui.adapters.DialogsAdapter;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class HistoryFragment extends Fragment implements DialogsAdapter.OnItemListener {
    private static final String TAG = "HISTORY_FRAGMENT";
    private static final String ARG_START_TYPE = "start_type";

    private final static int START_HISTORY = 0;
    private final static int START_BID = 1;
    private final static int START_ALL = 2;
    private final static int START_ACTIVE = 3;
    private final static int START_CLOSED = 4;
    private final static int NO_START = -1;

    private RecyclerView mListRV;
    private TextView mEmptyListTV;
    private RequestQueue queue;
    private int mStartType = NO_START;
    private ProgressBar mProgressBar;


    private Response.Listener<String> mDialogResponseListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {

            mListRV.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);

            try {
                JSONObject jsonResponse = new JSONObject(response);
                parseResponse( jsonResponse.getString("data") );
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    };

    public HistoryFragment() {
        // Required empty public constructor
    }

    public static HistoryFragment newInstanceActive() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_TYPE, START_ACTIVE);
        fragment.setArguments(args);
        return fragment;
    }

    public static HistoryFragment newInstanceClosed() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_TYPE, START_CLOSED);
        fragment.setArguments(args);
        return fragment;
    }

    public static HistoryFragment newInstanceHistory() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_TYPE, START_HISTORY);
        fragment.setArguments(args);
        return fragment;
    }

    public static HistoryFragment newInstanceAll() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_TYPE, START_ALL);
        fragment.setArguments(args);
        return fragment;
    }

    public static HistoryFragment newInstanceBid() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_TYPE, START_ACTIVE);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStartType = getArguments().getInt(ARG_START_TYPE, -1);
        }

        if (mStartType == NO_START)
            throw new Error("Has lost arguments, use factory method for creation fragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showActionBar();

        User currentUser = AstrologyApplication.getInstance().getCurrentUser();

        if (currentUser.getRole() == User.ROLE_USER)
            setTitleActionBar(getString(R.string.history));
        else if (currentUser.getRole() == User.ROLE_EXPERT)
            setTitleActionBar(getString(R.string.title_profile));

        mListRV = view.findViewById(R.id.rv_list);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mEmptyListTV = view.findViewById(R.id.tv_empty_list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        mListRV.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onStart() {
        super.onStart();
        mListRV.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        requestLoadDialogs();
    }


    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            String str = new String(error.networkResponse.data);
            //Log.d("HISTORY_LIST", str);
            //Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
        }
    };

    private void requestLoadDialogs() {
        queue = Volley.newRequestQueue(getContext());
        long userID = AstrologyApplication.getInstance().getCurrentUser().getServerID();
        DialogRequest dialogRequest;// = null;

        if (mStartType == START_ACTIVE)
            dialogRequest = DialogRequest.loadActive(userID, mDialogResponseListener, errorListener);
        else if (mStartType == START_CLOSED)
            dialogRequest = DialogRequest.loadClosed(userID, mDialogResponseListener, errorListener);
        else
            dialogRequest = DialogRequest.loadAll(userID, mDialogResponseListener, errorListener);

        dialogRequest.setTag(TAG);
        queue.add(dialogRequest);
    }

    private void parseResponse(String jsonDialogs) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        List<Dialog> list = gson.fromJson(jsonDialogs, new TypeToken<ArrayList<Dialog>>() {}.getType());

        if (list.isEmpty())
            mEmptyListTV.setVisibility(View.VISIBLE);
        else
            mEmptyListTV.setVisibility(View.GONE);

        DialogsAdapter dialogsAdapter = DialogsAdapter.createAdapter(list, this);
        mListRV.setAdapter(dialogsAdapter);
    }

    @Override
    public void onItemInteraction(Dialog dialog) {
        User currentUser = AstrologyApplication.getInstance().getCurrentUser();

        Intent intent = MessagesActivity.createIntent(getContext(), dialog, /*dialog.getExpertID(), dialog.getServerID(),*/ currentUser, dialog.getReceiver());
        startActivity(intent);
    }


    protected void setTitleActionBar(String title) {
        MainActivity activity = (MainActivity)getActivity();
        activity.setCustomTitle(title);
    }


    protected void showActionBar() {
        MainActivity activity = (MainActivity)getActivity();
        activity.getSupportActionBar().show();
    }
}
