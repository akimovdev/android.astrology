package com.akimovsoft.astrology.model.requests;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import java.util.HashMap;
import java.util.Map;

public class ExpertRequest extends StringRequest {

    public ExpertRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public static ExpertRequest loadExpertsByUser(long userID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new ExpertRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"experts/user/"+userID, listener, errorListener);
    }

    public static ExpertRequest loadExpertsByCategory(long userID, long categoryID, String sort, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new ExpertRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"experts/category/"+categoryID+"?user_id="+userID+"&sort="+sort, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Accept", "application/json");
        params.put("Authorization", AstrologyApplication.getInstance().getToken());
        return params;
    }
}
