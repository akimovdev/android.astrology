package com.akimovsoft.astrology.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;/*
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;*/
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.ImageJsonRequest;
import com.akimovsoft.astrology.model.requests.UserJsonRequest;
import com.akimovsoft.astrology.ui.activities.MainActivity;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.nostra13.universalimageloader.core.ImageLoader;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserProfileFragment extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int mHourOfDay = -1;
    private int mMinute = -1;
    private int mDay = -1;
    private int mMonth = -1;
    private int mYear = -1;

    private CircleImageView mAvatarCIV;
    private EditText mDateBirthET;
    private EditText mTimeBirthET;

    private PersonalDataFragment.OnPickerListener mPickerListener = new PersonalDataFragment.OnPickerListener() {
        @Override
        public void onTimeSet(int hourOfDay, int minute) {
            mHourOfDay = hourOfDay;
            mMinute = minute;

            fillTimeField();
        }

        @Override
        public void onDateSet(int year, int month, int day) {
            mYear = year;
            mMonth = month;
            mDay = day;

            fillDateField();
        }
    };

    protected void fillTimeField() {

        String hour;
        String minute;

        if (mHourOfDay < 10) {
            hour = "0"+mHourOfDay;
        } else {
            hour = String.valueOf(mHourOfDay);
        }

        if (mMinute < 10) {
            minute = "0"+mMinute;
        } else {
            minute = String.valueOf(mMinute);
        }

        String time = hour + " : " + minute;
        mTimeBirthET.setText(time);
    }

    protected void fillDateField() {

        String day;
        String month;

        if (mDay < 10) {
            day = "0"+mDay;
        } else {
            day = String.valueOf(mDay);
        }

        if (mMonth < 10) {
            month = "0"+mMonth;
        } else {
            month = String.valueOf(mMonth);
        }

        String date = day + "." + month + "." + mYear;
        mDateBirthET.setText(date);
    }

    public UserProfileFragment() {
        // Required empty public constructor
    }

    public static UserProfileFragment newInstance(/*String param1, String param2*/) {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_profile, container, false);
    }

    TextView mNameTV;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = AstrologyApplication.getInstance().getCurrentUser();

        mNameTV = view.findViewById(R.id.tv_name);
        final EditText nameET = view.findViewById(R.id.et_name);
        Button saveBTN =  view.findViewById(R.id.btn_save);

        final EditText cityLiveET = view.findViewById(R.id.et_city_live);
        final EditText cityBirthET = view.findViewById(R.id.et_city_birth);
        mDateBirthET = view.findViewById(R.id.et_date_birth);
        mTimeBirthET = view.findViewById(R.id.et_time_birth);

        InputFilter filterTxtLive = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i))) {
                        return cityLiveET.getText().toString();
                    }
                }
                return null;
            }
        };

        InputFilter filterTxtBirth = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i))) {
                        return cityBirthET.getText().toString();
                    }
                }
                return null;
            }
        };

        cityLiveET.setFilters(new InputFilter[]{filterTxtLive});
        cityBirthET.setFilters(new InputFilter[]{filterTxtBirth});

        View dateClickZone = view.findViewById(R.id.date_click_zone);
        dateClickZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new PersonalDataFragment.DatePickerFragment(mPickerListener);
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        View timeClickZone = view.findViewById(R.id.time_click_zone);
        timeClickZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PersonalDataFragment.TimePickerFragment newFragment = new PersonalDataFragment.TimePickerFragment(mPickerListener);
                newFragment.show(getFragmentManager(), "timePicker");
            }
        });

        mAvatarCIV = view.findViewById(R.id.iv_avatar);
        mAvatarCIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        long birthTimestamp = user.getBirthTimestamp();

        String date = getDate(birthTimestamp);
        String time = getTime(birthTimestamp);

        mNameTV.setText(user.getName());
        nameET.setText(user.getName());

        mDateBirthET.setText(date);
        mTimeBirthET.setText(time);

        cityLiveET.setText(user.getCityLive());
        cityBirthET.setText(user.getCityBirth());

        String imageUri = user.getUrlAvatar();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(imageUri, mAvatarCIV);

        saveBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                requestPutPersonalData(getBirthTimestamp(),
                        nameET.getText().toString(),
                        cityBirthET.getText().toString(),
                        cityLiveET.getText().toString());
            }
        });
    }

    private String getTime(long timestamp) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timestamp * 1000);

        mHourOfDay = calendar.get(GregorianCalendar.HOUR_OF_DAY);
        mMinute = calendar.get(GregorianCalendar.MINUTE);

        return mHourOfDay+":"+mMinute;
    }

    private String getDate(long timestamp) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timestamp * 1000);

        mYear = calendar.get(GregorianCalendar.YEAR);
        mMonth = calendar.get(GregorianCalendar.MONTH);
        mDay = calendar.get(GregorianCalendar.DAY_OF_MONTH);
        mMonth++;

        String month;

        if (mMonth < 10)
            month = "0" + mMonth;
        else
            month = String.valueOf(mMonth);

        return mDay+"/"+month+"/"+mYear;
    }

    private void requestPutPersonalData(long birthTimestamp, String name, String cityBirth, String cityLive) {

        //Long userID = AstrologyApplication.getInstance().getCurrentUser();//.getServerID();

        User user = AstrologyApplication.getInstance().getCurrentUser();
        user.setName(name);
        user.setBirthTimestamp(birthTimestamp);
        user.setCityBirth(cityBirth);
        user.setCityLive(cityLive);

        JSONObject bodyJSONObj = new JSONObject();

        try{
            bodyJSONObj.put(Constants.KEY_JSON_BIRTH_TIME, birthTimestamp);
            bodyJSONObj.put(Constants.KEY_JSON_NAME, name);
            bodyJSONObj.put(Constants.KEY_JSON_BIRTH_CITY, cityBirth);
            bodyJSONObj.put(Constants.KEY_JSON_LIVE_CITY, cityLive);
        }catch (JSONException e){
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        UserJsonRequest request = UserJsonRequest.edit(user.getServerID(), bodyJSONObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    handleResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_DUPLICATE) {
                    Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                }
            }
        });

        queue.add(request);
    }

    private void handleResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);

            if (isSuccess) {
                User user = AstrologyApplication.getInstance().getCurrentUser();
                user.saveAll();

                mNameTV.setText(user.getName());
                hideProgressDialog();
                //mListener.onPersonalDataSuccessful();
            }
        }
    }

    private long getBirthTimestamp() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, mYear);
        cal.set(Calendar.MONTH, mMonth);
        cal.set(Calendar.DAY_OF_MONTH, mDay);
        cal.set(Calendar.HOUR_OF_DAY, mHourOfDay);
        cal.set(Calendar.MINUTE, mMinute);

        return cal.getTimeInMillis()/1000;
    }

    protected void hideActionBar() {
        MainActivity activity = (MainActivity)getActivity();
        activity.getSupportActionBar().hide();
    }

    @Override
    protected void onOpenGalleryResult(int resultCode, Intent data) {
        super.onOpenGalleryResult(resultCode, data);

        if (resultCode == Activity.RESULT_OK)
            sendImageToServer(data);
    }

    private void sendImageToServer(Intent data) {

        Uri uri = data.getData();

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 75, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            showProgressDialog();
            requestLoadAvatar("Base64, "+encodedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestLoadAvatar(String imageBase64) {

        Long userID = AstrologyApplication.getInstance().getCurrentUser().getServerID();
        //Long userID = 51l;//AstrologyApplication.getInstance().getCurrentUser().getServerID();
        JSONObject bodyJSONObj = new JSONObject();

        try{
            bodyJSONObj.put(Constants.KEY_JSON_IMAGE, imageBase64);
        }catch (JSONException e){
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        ImageJsonRequest request = ImageJsonRequest.uploadUserAvatar(userID, imageBase64, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    handleUploadAvatarResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_DUPLICATE) {
                    Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                } if (error.networkResponse.statusCode == Constants.CODE_RESPONSE_SERVER_ERROR) {
                    Log.d("AVATAR_ERROR",new String(error.networkResponse.data));
                    Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getActivity(), getString(R.string.email_duplicate), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_SHORT).show();
                }
            }
        });

        queue.add(request);
    }

    private void handleUploadAvatarResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);

            if (isSuccess) {

                String urlAvatar = response.getJSONObject(Constants.KEY_JSON_DATA).getString(Constants.KEY_JSON_URL_AVATAR);
                User currentUser = AstrologyApplication.getInstance().getCurrentUser();
                currentUser.setUrlAvatar(urlAvatar);
                currentUser.saveAll();

                //mPhotoButtonRL.setVisibility(View.GONE);
                //mAvatarCIV.setVisibility(View.VISIBLE);

                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(currentUser.getUrlAvatar(), mAvatarCIV);
                hideProgressDialog();
                //Toast.makeText(getContext(), "SUCCESSFUL", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
