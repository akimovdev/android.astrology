package com.akimovsoft.astrology.ui.activities;

import android.os.Bundle;

import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.fragments.HistoryFragment;

public class HistoryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        replaceFragment(HistoryFragment.newInstanceHistory());
    }
}
