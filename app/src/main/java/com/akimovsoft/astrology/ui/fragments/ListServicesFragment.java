package com.akimovsoft.astrology.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
/*
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;*/
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Service;
import com.akimovsoft.astrology.model.requests.CategoryRequest;
import com.akimovsoft.astrology.ui.ItemOffsetDecoration;
import com.akimovsoft.astrology.ui.activities.ExpertsActivity;
import com.akimovsoft.astrology.ui.activities.MainActivity;
import com.akimovsoft.astrology.ui.adapters.ServicesAdapter;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ListServicesFragment extends Fragment implements ServicesAdapter.OnItemListener {
    private static final String TAG = "DASHBOARD_FRAGMENT";

    private RecyclerView rvServices;
    private RequestQueue queue;
    private ProgressBar mProgressBar;
    private TextView mEmptyListTV;

    public ListServicesFragment() {
        // Required empty public constructor
    }


    public static ListServicesFragment newInstance() {
        ListServicesFragment fragment = new ListServicesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_services, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showActionBar();
        setTitleActionBar(getString(R.string.list_of_services));

        int spanCount = view.getContext().getResources().getInteger(R.integer.cell_services_menu);
        rvServices = view.findViewById(R.id.rv_services);
        rvServices.setLayoutManager(new GridLayoutManager(view.getContext(), spanCount));

        mProgressBar = view.findViewById(R.id.progress_bar);
        mEmptyListTV = view.findViewById(R.id.tv_empty_list);

        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(view.getContext(), R.dimen.offset_item);
        rvServices.addItemDecoration(itemDecoration);

        requestLoadCategories();
    }

    private void requestLoadCategories() {
        queue = Volley.newRequestQueue(getContext());

        CategoryRequest categoryRequest = CategoryRequest.loadCategories (new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressBar.setVisibility(View.GONE);
                //rvServices.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    parseResponse( jsonResponse.getString("data") );
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String str = null;
                //Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        categoryRequest.setTag(TAG);
        queue.add(categoryRequest);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }


    private void parseResponse(String jsonCategories) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        List<Service> list = gson.fromJson(jsonCategories, new TypeToken<ArrayList<Service>>() {}.getType());

        if (list.isEmpty())
            mEmptyListTV.setVisibility(View.VISIBLE);

        ServicesAdapter servicesAdapter = ServicesAdapter.createAdapter(list, this);
        rvServices.setAdapter(servicesAdapter);
    }


    private static List<Service> debugGetServices() {
        List<Service> list = new ArrayList<>();

        list.add(new Service("Service 1"));
        /*list.add(new Service("Service 2"));
        list.add(new Service("Service 3"));
        list.add(new Service("Service 4"));
        list.add(new Service("Service 5"));
        list.add(new Service("Service 6"));
        list.add(new Service("Service 7"));
        list.add(new Service("Service 8"));
        list.add(new Service("Service 9"));*/

        return list;
    }

    @Override
    public void onItemInteraction(Service service) {
        Intent intent = ExpertsActivity.createIntentByCategoryID(getActivity(), service.getId());
        startActivity(intent);
    }

    protected void setTitleActionBar(String title) {
        MainActivity activity = (MainActivity)getActivity();
        activity.setCustomTitle(title);
    }

    protected void showActionBar() {
        MainActivity activity = (MainActivity)getActivity();
        activity.getSupportActionBar().show();
    }
}
