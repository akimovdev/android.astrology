package com.akimovsoft.astrology.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.model.entity.Message;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.DialogJsonRequest;
import com.akimovsoft.astrology.model.requests.MessageJsonRequest;
import com.akimovsoft.astrology.model.requests.MessageRequest;
import com.akimovsoft.astrology.ui.activities.FeedbackActivity;
import com.akimovsoft.astrology.ui.activities.MessagesActivity;
import com.akimovsoft.astrology.ui.adapters.MessengerAdapter;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesFragment extends BaseFragment {

    private String TAG = "MESSAGES_FRAGMENT";

    private String mChatKey;
    private DatabaseReference myRef;

    private RecyclerView messagesRV;
    private EditText mMessageET;
    private TextView mStateDialogTV;
    private ProgressBar mProgressBar;

    private User mUserSender;
    private User mUserReceiver;
    private Dialog mDialog;

    private RequestQueue mQueue;
    private boolean hasTurn = false;
    private Message mLastSendMessage;

    private ImageView mSendIV;
    private ImageView mAttachmentIV;

    public MessagesFragment() {}

    public static MessagesFragment newInstance(Dialog dialog, User userSender, User userReceiver) {
        MessagesFragment fragment = new MessagesFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.mUserSender = userSender;
        fragment.mUserReceiver = userReceiver;
        fragment.mDialog = dialog;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mChatKey = mDialog.getDid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("dialogs").child(mChatKey);
        myRef.addValueEventListener(postListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_messages, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        Context context = view.getContext();
        mProgressBar = view.findViewById(R.id.progress_bar);
        mStateDialogTV = view.findViewById(R.id.tv_closed_dialog);

        if (mUserSender.getRole() == User.ROLE_EXPERT)
            mStateDialogTV.setVisibility(View.GONE);

        if (mDialog.getState() == Dialog.STATE_ACTIVE || mDialog.getState() == Dialog.STATE_CLOSE)
            mProgressBar.setVisibility(View.VISIBLE);

        User currentUser = AstrologyApplication.getInstance().getCurrentUser();

        ((MessagesActivity)getActivity()).setCustomTitle(mUserReceiver.getName());

        messagesRV = view.findViewById(R.id.rvSendItems);
        mMessageET = view.findViewById(R.id.et_message);

        mAttachmentIV = view.findViewById(R.id.iv_attachment);

        mSendIV = view.findViewById(R.id.ivSend);
        mSendIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (hasTurn) {
                    Toast.makeText(getContext(), getString(R.string.alert_message_turn), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mMessageET.getText().toString().isEmpty()) {
                    Toast.makeText(view.getContext(), getString(R.string.alert_enter_message), Toast.LENGTH_SHORT).show();
                    return;
                } else if(!isNetworkConnected()) {
                    Toast.makeText(view.getContext(), getString(R.string.alert_sending_package_disable), Toast.LENGTH_SHORT).show();
                    return;
                }

                hasTurn = true;

                sendMessage(mMessageET.getText().toString());
            }
        });

        mAttachmentIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setStackFromEnd(true);
        messagesRV.setLayoutManager(linearLayoutManager);

        if (mDialog.getState() == Dialog.STATE_ACTIVE) {
            requestLoadMessages(mDialog.getServerID());
        } else if (mDialog.getState() == Dialog.STATE_CLOSE) {
            requestLoadMessages(mDialog.getServerID());
        }

        MessengerAdapter adapter = new MessengerAdapter(getActivity(), mUserSender, mUserReceiver, new ArrayList<Message>());
        messagesRV.setAdapter(adapter);
    }

    private void sendBroadcastState() {
        Intent intent = new Intent(Constants.BROADCAST_STATUS_DIALOG);
        intent.putExtra(Constants.EXTRA_DIALOG_SERVER_ID, mDialog.getServerID());
        intent.putExtra(Constants.EXTRA_DIALOG_DID, mDialog.getDid());
        intent.putExtra(Constants.EXTRA_DIALOG_STATUS, mDialog.getState());

        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    private void onAcceptBidViewCreated() {

        mQueue = Volley.newRequestQueue(getContext());
        DialogJsonRequest dialogJsonRequest
                = DialogJsonRequest
                .activeDialog(mDialog.getServerID(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            boolean success = response.getBoolean("success");

                            if (success) {
                                mDialog.setState(Dialog.STATE_ACTIVE);
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        dialogJsonRequest.setTag(TAG);
        mQueue.add(dialogJsonRequest);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mQueue != null) {
            mQueue.cancelAll(TAG);
        }
    }

    @Override
    protected void onOpenGalleryResult(int resultCode, Intent data) {
        super.onOpenGalleryResult(resultCode, data);

        if (resultCode == Activity.RESULT_OK)
            sendImageToServer(data);
    }

    private void sendImageToServer(Intent data) {

        Uri uri = data.getData();

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 75, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            showProgressDialog();
            sendImageMessage(encodedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void sendImageMessage(final String imageBase64) {
        Message message = new Message();
        message.setOwner(mUserSender);
        message.setState(Message.STATE_PROGRESS);

        requestSendMessage(message, "Base64, "+imageBase64);
    }


    private void sendMessage(final String messageBody) {
        Message message = new Message();
        message.setOwner(mUserSender);
        message.setState(Message.STATE_PROGRESS);
        message.setMessage(messageBody);

        requestSendMessage(message, null);
    }

    private void sendToFirebase(Message message) {
        myRef.setValue(buildDialogMap(message));
    }

    private Map<String, Object> buildDialogMap(Message message) {
        Map<String, Object> dialogMap = new HashMap<>();
        Map<String, Object> messageMap = new HashMap<>();
        Map<String, Object> userMap = new HashMap<>();

        userMap.put("id", message.getOwner().getServerID());
        userMap.put("name", message.getOwner().getName());
        userMap.put("email", message.getOwner().getEmail());
        userMap.put("url_avatar", message.getOwner().getUrlAvatar());

        messageMap.put("id", message.getServerID());
        messageMap.put("message", message.getMessage());
        messageMap.put("created_time", message.getTimestamp());
        messageMap.put("user", userMap);
        messageMap.put("image_url", message.getImageUrl());

        dialogMap.put("last_send_time", message.getTimestamp());
        dialogMap.put("message", messageMap);
        dialogMap.put("state", mDialog.getState());
        dialogMap.put("id", mDialog.getServerID());

        return dialogMap;
    }

    private void putMessageToList(Message message) {

        if (message.getOwner().getServerID() == mUserReceiver.getServerID() && mStateDialogTV.getVisibility() == View.VISIBLE) {
            mStateDialogTV.setVisibility(View.GONE);
        }

        mMessageET.getText().clear();

        MessengerAdapter adapter = (MessengerAdapter) messagesRV.getAdapter();
        adapter.addMessage(message);

        messagesRV.scrollToPosition(messagesRV.getAdapter().getItemCount()-1);
    }

    ValueEventListener postListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            if (dataSnapshot.getValue() == null)
                return;

            long state = (long) dataSnapshot.child("state").getValue();
            long id = (long) dataSnapshot.child("id").getValue();

            if (mDialog.getServerID() != id) {
                return;
            }

            if (!dataSnapshot.hasChild("message")) {
                return;
            }

            String msg = (String) dataSnapshot.child("message").child("message").getValue();
            long time = (Long) dataSnapshot.child("message").child("created_time").getValue();
            long userServerID = (Long) dataSnapshot.child("message").child("user").child("id").getValue();
            String userName = (String) dataSnapshot.child("message").child("user").child("name").getValue();
            String userEmail = (String) dataSnapshot.child("message").child("user").child("email").getValue();
            String userUrlAvatar = (String) dataSnapshot.child("message").child("user").child("url_avatar").getValue();
            String imageUrl = (String) dataSnapshot.child("message").child("image_url").getValue();

            User user = new User();
            user.setName(userName);
            user.setServerID(userServerID);
            user.setEmail(userEmail);
            user.setUrlAvatar(userUrlAvatar);

            Message message = new Message();
            message.setMessage(msg);
            message.setTimestamp(time);
            message.setOwner(user);
            message.setImageUrl(imageUrl);

            if (mLastSendMessage == null || !mLastSendMessage.equals(message)) {
                putMessageToList(message);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Getting Post failed, log a message
            Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
        }
    };

    private boolean isNetworkConnected() {
        return true;
        //return getHostActivity().isNetworkConnected();
    }

    private void requestLoadMessages(long dialogID) {
        mQueue = Volley.newRequestQueue(getContext());
        mProgressBar.setVisibility(View.GONE);

        MessageRequest messageRequest = MessageRequest.createLoadMessagesRequest(dialogID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    onLoadMessagesResponse( jsonResponse.getString("data") );
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        messageRequest.setTag(TAG);
        mQueue.add(messageRequest);
    }


    private void requestSendMessage(final Message message, String imageBase64) {

        mQueue = Volley.newRequestQueue(getContext());

        MessageJsonRequest messageRequest = MessageJsonRequest.createSendMessageRequest(mDialog.getServerID(), message.getMessage(), imageBase64, mUserSender, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hasTurn = false;

                try {
                    message.setTimestamp(response.getJSONObject("data").getLong("created_time"));

                    if (response.getJSONObject("data").has("image_url")) {
                        String url = response.getJSONObject("data").getString("image_url");
                        if (!url.equals("null")) {
                            message.setImageUrl(url);
                        }
                    }

                    mLastSendMessage = message;
                    sendToFirebase(message);
                    putMessageToList(message);
                    hideProgressDialog();
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                hasTurn = false;
            }
        });

        messageRequest.setTag(TAG);
        mQueue.add(messageRequest);
    }

    private void onLoadMessagesResponse(String jsonMessages) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);

        Gson gson = builder.create();
        List<Message> list = gson.fromJson(jsonMessages, new TypeToken<ArrayList<Message>>() {}.getType());

        if (list.isEmpty() && mUserSender.getRole() == User.ROLE_USER)
            mStateDialogTV.setVisibility(View.VISIBLE);

        if (list.size() > 0)
            mLastSendMessage = list.get(list.size() - 1);

        MessengerAdapter adapter = new MessengerAdapter(getActivity(), mUserSender, mUserReceiver, list);
        messagesRV.setAdapter(adapter);
    }

    private void onCloseDialog() {
        Intent intent
                = FeedbackActivity
                .newCreationFeedbackIntent(getContext(),
                        mDialog.getExpertID(),
                        mDialog.getServerID(),
                        mUserSender.getServerID());

        startActivityForResult(intent, Constants.REQUEST_CLOSE_DIALOG);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        boolean closeDialogVisible
                = AstrologyApplication.getInstance().getCurrentUser().getRole() == User.ROLE_USER &&
                    mDialog.getState() == Dialog.STATE_ACTIVE;

        menu.findItem(R.id.action_close).setVisible(closeDialogVisible);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;

            case R.id.action_close:
                onCloseDialog();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.REQUEST_CLOSE_DIALOG) {
                getActivity().finish();
            }
        }
    }
}
