package com.akimovsoft.astrology.model.entity;

import com.akimovsoft.astrology.model.Service;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dialog {

    public static final int STATE_ACTIVE = 1;
    public static final int STATE_CLOSE = 0;
    //public static final int STATE_BID = 2;

    @Expose
    @SerializedName("id")
    private long dialogID;

    private String did;

    @Expose
    @SerializedName("user_sender")
    private User sender;

    @Expose
    @SerializedName("user_receiver")
    private User receiver;

    @Expose
    @SerializedName("category")
    private Service service;

    @Expose
    @SerializedName("experience")
    private int experience;

    @Expose
    @SerializedName("active")
    private boolean active;

    @Expose
    @SerializedName("state")
    private int state;

    @Expose
    @SerializedName("expert_id")
    private long expertID;

    public long getServerID() {
        return dialogID;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public Service getService() {
        return service;
    }

    public int getExperience() {
        return experience;
    }

    public long getExpertID() {
        return expertID;
    }

    public void setExpertID(long expertID) {
        this.expertID = expertID;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getDid() {
        return did;
    }

    public boolean isActive() {
        return active;
    }

    public int getState(){
        return state;
    }

    public void setState(int state) {
        this.state = state;
        active = state == STATE_ACTIVE;
    }

    public void setServerID(long serverID) {
        this.dialogID = serverID;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
