package com.akimovsoft.astrology.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.ui.fragments.dialogs.FilterDialogFragment.FilterEntity;
import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.ViewHolder> {

    private final OnItemListener listener;
    private final List<FilterEntity> items;

    public static FiltersAdapter createAdapter(List<FilterEntity> items, OnItemListener listener) {
        return new FiltersAdapter(items, listener);
    }

    private FiltersAdapter(List<FilterEntity> items, OnItemListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_simple, viewGroup, false);
        return new FiltersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {

        viewHolder.item = items.get(position);
        viewHolder.tvName.setText(viewHolder.item.getName());

        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onItemInteraction(viewHolder.item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemListener {
        void onItemInteraction(FilterEntity filterEntity);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView tvName;

        public FilterEntity item;

        public ViewHolder(View view) {
            super(view);

            mView = view;
            tvName = view.findViewById(R.id.tv_name);
        }
    }
}
