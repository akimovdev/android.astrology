package com.akimovsoft.astrology.model.entity;

import androidx.annotation.NonNull;

import com.akimovsoft.astrology.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message implements Comparable {

    public final static int STATE_NORMAL = 0;
    public final static int STATE_FAIL = 1;
    public final static int STATE_PROGRESS = 2;
    public final static int STATE_UNREAD = 3;
    private final static int NO_STATE = -1;

    @Expose
    @SerializedName("id")
    private long serverID;

    private String message;

    @Expose
    @SerializedName("image_url")
    private String imageUrl;

    private int state;

    @Expose
    @SerializedName("user")
    private User owner;

    @Expose
    @SerializedName("created_time")
    private long time;

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        /*if (imageUrl != null)
            return Constants.SERVER_PRODUCTION+imageUrl;

        return null;*/
        return imageUrl;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Message) {
            return ((Message) obj).getTimestamp() == getTimestamp();
        }
        return false;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Message m = (Message) o;
        return compareTo(m);
    }

    public long getServerID() {
        return serverID;
    }

    public void setTimestamp(long time) {
        this.time = time;
    }

    public long getTimestamp() {
        return time;
    }
}
