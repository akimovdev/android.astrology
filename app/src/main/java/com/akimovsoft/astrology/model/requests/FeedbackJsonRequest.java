package com.akimovsoft.astrology.model.requests;

//import android.support.annotation.Nullable;

import androidx.annotation.Nullable;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FeedbackJsonRequest extends JsonObjectRequest {

    private FeedbackJsonRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public static FeedbackJsonRequest createSendFeedbackRequest(long expertID, String commentary, int rating, long dialogID, long userID, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener){

        String url = Constants.SERVER_PRODUCTION_API +"feedbacks";

        JSONObject jsonBodyObj = new JSONObject();

        try{
            jsonBodyObj.put("expert_id", expertID);
            jsonBodyObj.put("commentary", commentary);
            jsonBodyObj.put("rating", rating);
            jsonBodyObj.put("dialog_id", dialogID);
            jsonBodyObj.put("user_id", userID);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new FeedbackJsonRequest(Method.POST, url, jsonBodyObj, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("Authorization", AstrologyApplication.getInstance().getToken());
        //headers.put("Authorization", Constants.DEBUG_TOKEN);

        return headers;
    }
}
