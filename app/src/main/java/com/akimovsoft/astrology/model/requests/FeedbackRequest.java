package com.akimovsoft.astrology.model.requests;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class FeedbackRequest extends StringRequest {

    public FeedbackRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public static FeedbackRequest loadFeedbacks(long expertID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return new FeedbackRequest(Method.GET, Constants.SERVER_PRODUCTION_API +"feedbacks/expert/"+expertID, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Accept", "application/json");
        //params.put("Authorization", Constants.DEBUG_TOKEN);
        params.put("Authorization", AstrologyApplication.getInstance().getToken());
        return params;
    }
}
