package com.akimovsoft.astrology.model.requests;

//import android.support.annotation.Nullable;

import androidx.annotation.Nullable;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class MessageRequest extends StringRequest {

    private MessageRequest(int method, String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public static MessageRequest createLoadMessagesRequest(long dialogID, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
        return new MessageRequest(Method.GET, Constants.SERVER_PRODUCTION_API + "messages/dialog/"+dialogID, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", AstrologyApplication.getInstance().getToken());
        return headers;
    }
}
