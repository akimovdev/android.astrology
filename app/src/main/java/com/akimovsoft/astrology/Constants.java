package com.akimovsoft.astrology;

public class Constants {
    public static final String APP_PREFERENCES = "app_preferences";
    //public static final String DEBUG_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0YzBiYWFlYi1iYThlLTQxOTEtYTA1YS04MTY4ZTRiZDhhNTUxIiwiZXhwIjoxNTYyNzY3NzAzfQ.zs228zOaBrqI3X1_vrH8v9n1vsbw2au62L9HsffVjrM";
    public  static final String SERVER_PRODUCTION_API = "http://sharisse.beget.tech/api/";
    public  static final String SERVER_PRODUCTION = "http://sharisse.beget.tech/";

    public  static final String EXTRA_CATEGORY_ID = "category_id";
    public  static final String EXTRA_EXPERT_SERVER_ID = "extra_expert_server_id";
    public  static final String EXTRA_DIALOG_SERVER_ID = "extra_dialog_server_id";
    public  static final String EXTRA_DIALOG_DID = "extra_dialog_did";
    public  static final String EXTRA_DIALOG_STATUS = "extra_dialog_status";
    public  static final String EXTRA_USER_SERVER_ID = "extra_user_server_id";
    public  static final String EXTRA_USER_NAME = "extra_user_name";
    public  static final String EXTRA_IMAGE_URL = "extra_image_url";
    public  static final String EXTRA_SERVICE_NAME = "extra_service_name";
    public static final String EXTRA_START_MODE = "extra_start_mode";
    public static final String EXTRA_DIALOG_ID = "extra_dialog_id";

    public static final String KEY_JSON_SUCCESS = "success";
    public static final String KEY_JSON_DATA = "data";
    public static final String KEY_JSON_USER = "user";
    public static final String KEY_JSON_TOKEN = "token";
    public static final String KEY_JSON_SERVER_ID = "id";
    public static final String KEY_JSON_DID = "did";
    public static final String KEY_JSON_NAME = "name";
    public static final String KEY_JSON_PUSH_TOKEN = "push_token";
    public static final String KEY_JSON_BIRTH_TIME = "birth_time";
    public static final String KEY_JSON_BIRTH_CITY = "birth_city";
    public static final String KEY_JSON_LIVE_CITY = "live_city";
    public static final String KEY_JSON_IMAGE = "image";
    public static final String KEY_JSON_URL_AVATAR = "url_avatar";

    public static final String BROADCAST_BID = "broadcast_bid";
    public static final String BROADCAST_DIALOG_CHANGE = "broadcast_dialog_change";
    public static final String BROADCAST_STATUS_DIALOG = "broadcast_status_dialog";

    public static final String KEY_FB_DIALOGS = "dialogs";

    public static final int CODE_RESPONSE_UNAUTHORISED = 401;
    public static final int CODE_RESPONSE_DUPLICATE = 422;
    public static final int CODE_RESPONSE_SERVER_ERROR = 500;

    public static final String PREF_CURRENT_USER_ID = "pref_current_user_id";
    public static final String PREF_CURRENT_USER_SERVER_ID = "pref_current_user_server_id";
    public static final String PREF_CURRENT_TOKEN = "pref_current_token";
    public static final String PREF_PUSH_TOKEN = "pref_push_token";

    public static final String MSG_LOST_START_MODE_ACTIVITY = "StartMode parameter has been lost. Please use factory method for start activity !!!";

    public static final int REQUEST_OPEN_GALLERY = 9999;
    public static final int REQUEST_CLOSE_DIALOG = 9998;

    //public static final String DEBUG_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0YzBiYWFlYi1iYThlLTQxOTEtYTA1YS04MTY4ZTRiZDhhNTUxIiwiZXhwIjoxNTY1MzY4ODM0fQ.X9rqj78k_8DxoE5pCp-7400Mv9zhYKDfmRXrxhVmuy4";
}
