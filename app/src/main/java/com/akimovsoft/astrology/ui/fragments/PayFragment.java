package com.akimovsoft.astrology.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Expert;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.model.requests.DialogJsonRequest;
import com.akimovsoft.astrology.ui.activities.BaseActivity;
import com.akimovsoft.astrology.ui.activities.MessagesActivity;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;

public class PayFragment extends BaseFragment {
    String TAG = "PAY_FRAGMENT";

    private Expert mExpert;
    private User mCurrentUser;
    private RequestQueue queue;

    public PayFragment() {
        // Required empty public constructor
    }

    public static PayFragment newInstance(Expert expert) {
        PayFragment fragment = new PayFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.mExpert = expert;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mCurrentUser = AstrologyApplication.getInstance().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pay, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((BaseActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_pay));

        TextView usernameTV = view.findViewById(R.id.tv_name);
        TextView experienceTV = view.findViewById(R.id.tv_experience);
        TextView priceTV = view.findViewById(R.id.tv_price);
        TextView serviceTV = view.findViewById(R.id.tv_service);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        Button payBtn = view.findViewById(R.id.btn_pay);

        usernameTV.setText(mExpert.getUser().getName());
        experienceTV.setText("Стаж "+mExpert.getExperience()+" лет");
        priceTV.setText(String.valueOf(mExpert.getPrice())+" ₽");
        serviceTV.setText(mExpert.getService().getName());
        ratingBar.setRating(mExpert.getRating());

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
                //createDialog();
                showProgressDialog();
            }
        });
    }

    private Dialog mDialog;

    private void createDialog() {

        mDialog = new Dialog();
        //mDialog.setState(Dialog.STATE_BID);
        mDialog.setExpertID(mExpert.getServerID());
        mDialog.setSender(mExpert.getUser());
        mDialog.setReceiver(mCurrentUser);

        requestCreateDialog(mCurrentUser.getServerID(), mExpert.getServerID(), "startDialog");
    }

    private void requestCreateDialog(long senderID, long expertID, String message) {
        queue = Volley.newRequestQueue(getContext());

        DialogJsonRequest dialogJsonRequest = DialogJsonRequest.createDialog(senderID, expertID, message, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    onCreateDialogResponse(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d("RESPONSE", new String(error.networkResponse.data));
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        });

        dialogJsonRequest.setTag(TAG);
        queue.add(dialogJsonRequest);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    private void onCreateDialogResponse(JSONObject response) throws JSONException {

        if (response.has(Constants.KEY_JSON_SUCCESS)) {
            boolean isSuccess = response.getBoolean(Constants.KEY_JSON_SUCCESS);
            long id = response.getJSONObject(Constants.KEY_JSON_DATA).getLong(Constants.KEY_JSON_SERVER_ID);
            String did = response.getJSONObject(Constants.KEY_JSON_DATA).getString(Constants.KEY_JSON_DID);

            mDialog.setServerID(id);
            mDialog.setDid(did);

            if (isSuccess) {
                sendBroadcast();
                sendPushNotification(mExpert.getUser().getPushToken());
                hideProgressDialog();
                getActivity().finish();
                Intent intent = MessagesActivity.createIntent(getContext(), mDialog, mCurrentUser, mExpert.getUser());
                startActivity(intent);
            }
        }
    }

    private void sendBroadcast() {
        Intent intent = new Intent(Constants.BROADCAST_STATUS_DIALOG);
        intent.putExtra(Constants.EXTRA_DIALOG_SERVER_ID, mDialog.getServerID());
        intent.putExtra(Constants.EXTRA_DIALOG_DID, mDialog.getDid());
        intent.putExtra(Constants.EXTRA_DIALOG_STATUS, Dialog.STATE_ACTIVE);

        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    private void sendPushNotification(String pushToken) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
