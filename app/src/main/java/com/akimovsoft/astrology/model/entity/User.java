package com.akimovsoft.astrology.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.akimovsoft.astrology.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Table(name = "User")
public class User extends Model {

    public static final int ROLE_USER = 0;
    public static final int ROLE_EXPERT = 1;
    //public static final int ROLE_ADMIN = 2;

    @Expose
    @SerializedName("id")
    @Column(name = "ServerID")
    private long serverID;

    @Expose
    @Column(name = "Name")
    private String name;

    @Expose
    @Column(name = "Email")
    private String email;

    @Expose
    @Column(name = "Nickname")
    private String nickname;

    @Expose
    @SerializedName("birth_time")
    @Column(name = "BirthTime")
    private long birthTimestamp;

    @Expose
    @SerializedName("push_token")
    @Column(name = "PushToken")
    private String pushToken;

    /*
    @Expose
    @SerializedName("birth_city")
    @Column(name = "CityBirth")
    private City cityBirth;

    @Expose
    @SerializedName("live_city")
    @Column(name = "CityLive")
    private City cityLive;*/

    @Expose
    @SerializedName("birth_city")
    @Column(name = "CityBirth")
    private String cityBirth;

    @Expose
    @SerializedName("live_city")
    @Column(name = "CityLive")
    private String cityLive;

    @Expose
    @SerializedName("url_avatar")
    @Column(name = "UrlAvatar")
    private String urlAvatar;

    @Expose
    @SerializedName("role")
    @Column(name = "Role")
    private int role;

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        if (name == null) {
            if (nickname == null) {
                return "No Name";
            }
            return nickname;
        }

        return name;
    }

    public String getEmail() {
        return email;
    }

    public long getServerID() {
        return serverID;
    }

    public void setServerID(long serverID) {
        this.serverID = serverID;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setCityBirth(String cityBirth) {
        this.cityBirth = cityBirth;
    }

    public void setCityLive(String cityLive) {
        this.cityLive = cityLive;
    }

    public String getCityBirth() {
        return cityBirth;
    }

    public String getCityLive() {
        return cityLive;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public void setBirthTimestamp(long birthTimestamp) {
        this.birthTimestamp = birthTimestamp;
    }

    public String getPushToken() {
        return pushToken;
    }

    public long getBirthTimestamp() {
        return birthTimestamp;
    }

    public String getUrlAvatar() {
        return Constants.SERVER_PRODUCTION+urlAvatar;
    }

    public int getRole() {
        return role;
    }

    public Long saveAll() {

        /*
        if (cityLive != null)
            cityLive.save();

        if (cityBirth != null)
            cityBirth.save();*/

        return save();
    }
}
