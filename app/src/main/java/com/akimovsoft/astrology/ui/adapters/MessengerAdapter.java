package com.akimovsoft.astrology.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.entity.Message;
import com.akimovsoft.astrology.model.entity.User;
import com.nostra13.universalimageloader.core.ImageLoader;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessengerAdapter extends RecyclerView.Adapter<MessengerAdapter.ViewHolder> {

    private int COUNT_ITEMS_LOAD = 60;
    private List<Message> messageList = new ArrayList<>();
    private User currentUser;

    public MessengerAdapter(Context context, User userSender, User userReceiver, List<Message> messageList) {
        this.messageList.addAll(messageList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(viewType, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.mItem = messageList.get(position);
        User user = viewHolder.mItem.getOwner();
        viewHolder.mContentView.setText(messageList.get(position).getMessage());

        if (viewHolder.mItem.getImageUrl() != null) {
            viewHolder.imageIV.setVisibility(View.VISIBLE);
            viewHolder.mContentView.setVisibility(View.GONE);

            String imageUrl = Constants.SERVER_PRODUCTION+viewHolder.mItem.getImageUrl();

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(imageUrl, viewHolder.imageIV);
        } else {
            viewHolder.imageIV.setVisibility(View.GONE);
            viewHolder.mContentView.setVisibility(View.VISIBLE);
        }

        boolean isReceiver = !AstrologyApplication.getInstance().isCurrentUser(user);

        if (isReceiver) {
            if (position > 0) {
                Message previousMessage = messageList.get(position - 1);

                if (previousMessage.getOwner().getServerID() == user.getServerID()) {
                    viewHolder.civAvatar.setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.civAvatar.setVisibility(View.VISIBLE);
                }
            } else {
                viewHolder.civAvatar.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
            return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {

        User user = messageList.get(position).getOwner();

        if (AstrologyApplication.getInstance().isCurrentUser(user))
            return R.layout.item_message_right;
        else
            return R.layout.item_message_left;
    }

    public void addMessage(Message message) {

        if (messageList.contains(message))
            return;

        messageList.add(message);
        notifyItemInserted(messageList.size() - 1);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public Message mItem;
        public final TextView mContentView;
        public final CircleImageView civAvatar;
        public final ImageView imageIV;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.tvMessage);
            civAvatar = view.findViewById(R.id.civ_avatar);
            imageIV = view.findViewById(R.id.iv_image);
        }
    }
}
