package com.akimovsoft.astrology.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Expert;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class ExpertsAdapter extends RecyclerView.Adapter<ExpertsAdapter.ViewHolder> {

    private static final int START_FULL = 0;
    private static final int START_MIN = 1;

    private static final int INGOT_VIEW_TYPE = 0;
    private static final int FULL_VIEW_TYPE = 1;
    private static final int MIN_VIEW_TYPE = 2;

    private final OnItemListener listener;
    private final List<Expert> items;
    //private int layout;
    private int mStartType;


    public static ExpertsAdapter createFullDataAdapter(List<Expert> items, OnItemListener listener) {
        List<Expert> itemsFirst = new ArrayList<>();
        itemsFirst.add(new Expert());
        itemsFirst.addAll(items);
        return new ExpertsAdapter (itemsFirst, START_FULL, R.layout.item_expert_full, listener);
    }

    public static ExpertsAdapter createMinDataAdapter(List<Expert> items, OnItemListener listener) {
        return new ExpertsAdapter (items, START_MIN, R.layout.item_expert_min, listener);
    }

    private ExpertsAdapter (List<Expert> items, int startType, int layout, OnItemListener listener) {
        this.items = items;
        this.listener = listener;
        //this.layout = layout;
        this.mStartType = startType;
    }

    @Override
    public int getItemViewType(int position) {

        if (mStartType == START_FULL) {
            if (position == 0)
                return INGOT_VIEW_TYPE;

            return FULL_VIEW_TYPE;
        }

        return MIN_VIEW_TYPE;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int layout;

        if (i == INGOT_VIEW_TYPE)
            layout = R.layout.item_expert_empty;
        else if (i == FULL_VIEW_TYPE)
            layout = R.layout.item_expert_full;
        else
            layout = R.layout.item_expert_min;


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(layout, viewGroup, false);
        return new ExpertsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        viewHolder.item = items.get(i);

        if (mStartType == START_FULL)
            onBindFullData(viewHolder, i);
        else
            onBindMinData(viewHolder, i);


        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onItemInteraction(viewHolder.item);
                }
            }
        });
    }

    private void onBindFullData(@NonNull final ViewHolder viewHolder, int i) {

        if (i == 0) return;

        viewHolder.tvName.setText(viewHolder.item.getName());
        viewHolder.tvPrice.setText(String.valueOf(viewHolder.item.getPrice())+" ₽");
        viewHolder.tvExperience.setText("Стаж "+viewHolder.item.getExperience()+" лет");
        viewHolder.tvService.setText(viewHolder.item.getService().getName());
        viewHolder.ratingBar.setRating(viewHolder.item.getRating());

        if (viewHolder.item.getDialog() == null) {
            viewHolder.btnOpen.setText("Заказать услугу");
        } else {
            viewHolder.btnOpen.setText("Продолжить диалог");
        }

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(viewHolder.item.getUser().getUrlAvatar(), viewHolder.civAvatar);
    }

    private void onBindMinData(@NonNull final ViewHolder viewHolder, int i) {
        viewHolder.tvService.setText(viewHolder.item.getService().getName());
        viewHolder.tvPrice.setText(String.valueOf(viewHolder.item.getPrice())+" ₽");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addData(List<Expert> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        int size = items.size();
        items.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface OnItemListener {
        void onItemInteraction(Expert game);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView tvName;
        public final RatingBar ratingBar;
        public final TextView tvExperience;
        public final TextView tvService;
        public final TextView tvPrice;
        public final CircleImageView civAvatar;
        public final Button btnOpen;

        public Expert item;

        public ViewHolder(View view) {
            super(view);

            mView = view;
            tvName = view.findViewById(R.id.tv_name);
            ratingBar = view.findViewById(R.id.ratingBar);
            tvExperience = view.findViewById(R.id.tv_experience);
            tvService = view.findViewById(R.id.tv_service);
            tvPrice = view.findViewById(R.id.tv_price);
            civAvatar = view.findViewById(R.id.civ_avatar);
            btnOpen = view.findViewById(R.id.btn_open);
        }
    }
}
