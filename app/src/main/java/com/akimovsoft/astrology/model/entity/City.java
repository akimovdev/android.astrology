package com.akimovsoft.astrology.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Table(name = "City")
public class City extends Model {

    @Expose
    @SerializedName("city")
    @Column(name = "Name")
    String name;

    public String getName() {
        return name;
    }
}
