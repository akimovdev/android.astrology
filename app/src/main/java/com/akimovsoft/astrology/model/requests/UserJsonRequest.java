package com.akimovsoft.astrology.model.requests;

//import android.support.annotation.Nullable;

import androidx.annotation.Nullable;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;



public class UserJsonRequest extends JsonObjectRequest {

    final static String TAG = "USER_REQUEST";


    public UserJsonRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public static UserJsonRequest auth(String email, String password, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {

        JSONObject jsonBodyObj = new JSONObject();
        String url = Constants.SERVER_PRODUCTION_API +"users/auth";
        String pushToken = AstrologyApplication.getInstance().getPushToken();

        try{
            jsonBodyObj.put("email", email);
            jsonBodyObj.put("password", password);
            jsonBodyObj.put("push_token", pushToken);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new UserJsonRequest(Method.POST, url, jsonBodyObj, listener, errorListener);
    }

    public static UserJsonRequest register(String email, String password, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {

        JSONObject jsonBodyObj = new JSONObject();
        String url = Constants.SERVER_PRODUCTION_API +"users/register";
        try{
            jsonBodyObj.put("email", email);
            jsonBodyObj.put("password", password);
            jsonBodyObj.put("push_token", AstrologyApplication.getInstance().getPushToken());
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new UserJsonRequest(Method.POST, url, jsonBodyObj, listener, errorListener);
    }

    public static UserJsonRequest edit(long userID, JSONObject bodyJSONObj, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {

        String url = Constants.SERVER_PRODUCTION_API +"users/"+userID;
        return new UserJsonRequest(Method.PUT, url, bodyJSONObj, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        //headers.put("Authorization", Constants.DEBUG_TOKEN);

        if (AstrologyApplication.getInstance().getToken() != null)
            headers.put("Authorization", AstrologyApplication.getInstance().getToken());

        return headers;
    }

/*
    @Override    public byte[] getBody() {
        try {
            return requestBody == null ? null : requestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    requestBody, "utf-8");
            return null;
        }
    }*/
}
