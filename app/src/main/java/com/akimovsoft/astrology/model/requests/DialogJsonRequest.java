package com.akimovsoft.astrology.model.requests;

import androidx.annotation.Nullable;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DialogJsonRequest extends JsonObjectRequest {

    private DialogJsonRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public static DialogJsonRequest createDialog(long senderID, long expertID, String message, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {

        JSONObject jsonBodyObj = new JSONObject();
        String url = Constants.SERVER_PRODUCTION_API +"dialogs";
        try{
            jsonBodyObj.put("sender_id", senderID);
            jsonBodyObj.put("expert_id", expertID);
            jsonBodyObj.put("message", message);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new DialogJsonRequest(Method.POST, url, jsonBodyObj, listener, errorListener);
    }

    public static DialogJsonRequest activeDialog(long dialogID, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {

        JSONObject jsonBodyObj = new JSONObject();
        String url = Constants.SERVER_PRODUCTION_API +"dialogs/active/"+dialogID;

        try{
            jsonBodyObj.put("state", Dialog.STATE_ACTIVE);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return new DialogJsonRequest(Method.PUT, url, jsonBodyObj, listener, errorListener);
    }

    public static DialogJsonRequest closeDialog(long dialogID, JSONObject jsonBodyObj, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = Constants.SERVER_PRODUCTION_API +"dialogs/close/"+dialogID;
        return new DialogJsonRequest(Method.PUT, url, jsonBodyObj, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("Authorization", AstrologyApplication.getInstance().getToken());

        return headers;
    }
}
