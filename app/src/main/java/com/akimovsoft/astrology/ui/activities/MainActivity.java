package com.akimovsoft.astrology.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Service;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.model.entity.User;
import com.akimovsoft.astrology.ui.fragments.HistoryFragment;
import com.akimovsoft.astrology.ui.fragments.ListServicesFragment;
import com.akimovsoft.astrology.ui.fragments.UserProfileFragment;
import com.akimovsoft.astrology.ui.fragments.dialogs.BidDialogFragment;
import com.akimovsoft.astrology.view.SlidingTabLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends BaseActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    private static final int[] TAB_ITEMS = new int[] { R.string.tab_active,
            R.string.tab_closed };

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private User mCurrentUser;

    private TextView mToolBarTV;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String userName = intent.getStringExtra(Constants.EXTRA_USER_NAME);
            String categoryName = intent.getStringExtra(Constants.EXTRA_SERVICE_NAME);
            String avatarURL = intent.getStringExtra(Constants.EXTRA_IMAGE_URL);
            String did = intent.getStringExtra(Constants.EXTRA_DIALOG_DID);
            long dialogServerID = intent.getLongExtra(Constants.EXTRA_DIALOG_SERVER_ID, -1);
            int state = intent.getIntExtra(Constants.EXTRA_DIALOG_STATUS, Dialog.STATE_ACTIVE);

            User userReceiver = new User();
            userReceiver.setName(userName);
            userReceiver.setUrlAvatar(avatarURL);

            Service service = new Service(categoryName);

            Dialog dialog = new Dialog();
            dialog.setState(state);
            dialog.setServerID(dialogServerID);
            dialog.setReceiver(userReceiver);
            dialog.setService(service);
            dialog.setDid(did);

            showDialog(dialog);
        }
    };

    public void showDialog(Dialog dialog) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        DialogFragment newFragment = BidDialogFragment.newInstance(dialog);
        newFragment.show(ft, "dialog");
    }

    private void initToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mToolBarTV = findViewById(R.id.toolbar_title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ( !auth() ) return;


        if (mCurrentUser.getRole() == User.ROLE_USER) {
            onCreateUser(savedInstanceState);
        } else {
            onCreateExpert(savedInstanceState);
        }
    }


    private void onCreateUser(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);

        initToolBar();

        if (savedInstanceState == null) {
            replaceFragment(ListServicesFragment.newInstance());
        }

        mToolBarTV.setText(getString(R.string.list_of_services));

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        navigation.setVisibility(View.VISIBLE);
    }


    private void onCreateExpert(Bundle savedInstanceState) {
        setContentView(R.layout.activity_expert_main);

        initToolBar();

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new SamplePagerAdapter(getSupportFragmentManager()));
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.tabs_text_bg, R.id.tabsText);
        mSlidingTabLayout.setViewPager(mViewPager);

        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return ContextCompat.getColor(MainActivity.this, R.color.colorPrimary);
            }

            @Override
            public int getDividerColor(int position) {
                return 0;
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(Constants.BROADCAST_BID));
    }


    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private boolean auth() {
        mCurrentUser = AstrologyApplication.getInstance().getCurrentUser();

        if (mCurrentUser == null) {
            startActivity(new Intent(this, AuthActivity.class));
            finish();
            return false;
        }
        return true;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.navigation_services:
                replaceFragment(ListServicesFragment.newInstance());
                return true;

            case R.id.navigation_articles:
                //replaceFragment(HistoryFragment.newInstanceAll());
                return false;

            case R.id.navigation_history:
                replaceFragment(HistoryFragment.newInstanceAll());
                return true;

            case R.id.navigation_profile:
                replaceFragment(UserProfileFragment.newInstance());
                return true;
        }

        return false;
    }


    public void setCustomTitle(String title) {
        mToolBarTV.setText(title);
    }

    private class SamplePagerAdapter extends FragmentStatePagerAdapter {

        SparseArray<Fragment> registeredFragments;

        SamplePagerAdapter(FragmentManager fm) {
            super(fm);
            registeredFragments = new SparseArray<>();
        }

        @Override
        public int getCount() {
            return TAB_ITEMS.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString( TAB_ITEMS[position] );
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0)
                return HistoryFragment.newInstanceActive();

            return HistoryFragment.newInstanceClosed();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisterFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
