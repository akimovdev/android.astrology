package com.akimovsoft.astrology.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Expert;
import com.akimovsoft.astrology.model.entity.Dialog;
import com.akimovsoft.astrology.ui.fragments.dialogs.ExpertProfileFragment;
import com.akimovsoft.astrology.ui.fragments.ListExpertsFragment;

public class ExpertsActivity extends BaseActivity
        implements ExpertProfileFragment.OnExpertProfileFragmentListener {

    private final static int START_LIST = 0;
    private final static int START_ERROR = -1;

    private ExpertProfileFragment mProfileFragment = null;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mProfileFragment == null)
                return;

            String did = intent.getStringExtra(Constants.EXTRA_DIALOG_DID);
            long dialogServerID = intent.getLongExtra(Constants.EXTRA_DIALOG_SERVER_ID, -1);
            int state = intent.getIntExtra(Constants.EXTRA_DIALOG_STATUS, -1);

            if (did == null || dialogServerID == -1 || state == -1) {
                throw new Error("Extra data has been lost");
            }

            Dialog dialog = new Dialog();
            dialog.setServerID(dialogServerID);
            dialog.setDid(did);
            dialog.setReceiver(mProfileFragment.getExpert().getUser());
            dialog.setSender(AstrologyApplication.getInstance().getCurrentUser());
            dialog.setState(state);
            dialog.setExpertID(mProfileFragment.getExpert().getServerID());

            mProfileFragment.getExpert().setDialog(dialog);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            //replaceFragment(ListExpertsFragment.newInstanceHistory());
            onModeCreate(getIntent().getIntExtra(Constants.EXTRA_START_MODE, START_ERROR));
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(Constants.BROADCAST_STATUS_DIALOG));
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private void onModeCreate(int mode) {

        if (mode == START_LIST) {
            long categoryID = getIntent().getLongExtra(Constants.EXTRA_CATEGORY_ID, -1);
            replaceFragment(ListExpertsFragment.newInstance(categoryID));
        } else {
            throw new Error(Constants.MSG_LOST_START_MODE_ACTIVITY);
        }
    }

    public void showFilterDialog(DialogFragment newFragment) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        //DialogFragment newFragment = FilterDialogFragment.newInstanceHistory();
        newFragment.show(ft, "dialog");
    }



    public void showProfileDialog(Expert expert) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        mProfileFragment = ExpertProfileFragment.newInstance(expert);

        DialogFragment newFragment = mProfileFragment;
        newFragment.show(ft, "dialog");
    }


    public static Intent createIntentByCategoryID(Context context, long categoryID) {
        Intent intent = new Intent(context, ExpertsActivity.class);
        intent.putExtra(Constants.EXTRA_START_MODE, START_LIST);
        intent.putExtra(Constants.EXTRA_CATEGORY_ID, categoryID);
        return intent;
    }


    @Override
    public void onDestroyExpertProfileFragment() {
        mProfileFragment = null;
    }
}
