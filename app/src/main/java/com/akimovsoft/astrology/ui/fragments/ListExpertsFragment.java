package com.akimovsoft.astrology.ui.fragments;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.akimovsoft.astrology.AstrologyApplication;
import com.akimovsoft.astrology.Constants;
import com.akimovsoft.astrology.R;
import com.akimovsoft.astrology.model.Expert;
import com.akimovsoft.astrology.model.requests.ExpertRequest;
import com.akimovsoft.astrology.ui.activities.ExpertsActivity;
import com.akimovsoft.astrology.ui.activities.MessagesActivity;
import com.akimovsoft.astrology.ui.adapters.ExpertsAdapter;
import com.akimovsoft.astrology.ui.adapters.FiltersAdapter;
import com.akimovsoft.astrology.ui.fragments.dialogs.FilterDialogFragment;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;


public class ListExpertsFragment extends Fragment implements ExpertsAdapter.OnItemListener {
    private static final String TAG = "LIST_EXPERTS_FRAGMENT";

    private static final String ARG_CATEGORY_ID = "category_id";

    private RecyclerView rvExperts;
    private ProgressBar mProgressBar;
    private TextView mEmptyListTV;
    private TextView mDescriptionTV;
    private ImageView mIconIV;
    private RequestQueue queue;
    private RelativeLayout mDescriptionRL;

    private long categoryID;
    private int mStartDashHeight;
    private int mListOffset;
    private ExpertsAdapter expertsAdapter;


    public ListExpertsFragment() {
        // Required empty public constructor
    }


    public static ListExpertsFragment newInstance(long categoryID) {
        ListExpertsFragment fragment = new ListExpertsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CATEGORY_ID, categoryID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() == null) {
            throw new Error("Use factory method!!!");
        }

        categoryID = getArguments().getLong(ARG_CATEGORY_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_expert, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgressBar = view.findViewById(R.id.progress_bar);
        mIconIV = view.findViewById(R.id.iv_icon);
        mDescriptionTV = view.findViewById(R.id.tv_description);

        rvExperts = view.findViewById(R.id.rv_experts);
        rvExperts.setLayoutManager(new LinearLayoutManager(view.getContext()));

        mEmptyListTV = view.findViewById(R.id.tv_empty_list);
        mDescriptionRL = view.findViewById(R.id.rl_description);
        mStartDashHeight = mDescriptionRL.getLayoutParams().height;

        rvExperts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                mListOffset += dy;

                ViewGroup.LayoutParams layoutParams = mDescriptionRL.getLayoutParams();

                if (mListOffset > mStartDashHeight) {
                    layoutParams.height = 0;
                } else {
                    int newHeight = layoutParams.height - dy;

                    if (newHeight > mStartDashHeight) {
                        newHeight = mStartDashHeight;
                    }
                    layoutParams.height = newHeight;
                }

                mDescriptionRL.setLayoutParams(layoutParams);
            }
        });

        final FiltersAdapter.OnItemListener itemFilterListener = new FiltersAdapter.OnItemListener() {
            @Override
            public void onItemInteraction(FilterDialogFragment.FilterEntity filterEntity) {
                getActivity().onBackPressed();
                requestLoadExperts(filterEntity.getSort());
            }
        };

        //Button filterBTN = view.findViewById(R.id.btn_filter);
        ImageView filterIV = view.findViewById(R.id.iv_filter);
        filterIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterDialogFragment.newInstance(itemFilterListener);
                ((ExpertsActivity)getActivity()).showFilterDialog(FilterDialogFragment.newInstance(itemFilterListener));
            }
        });
    }

    /*
    public void animation() {
        ValueAnimator anim = ValueAnimator.ofInt(mDashRL.getMeasuredHeight(), -100);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = mDashRL.getLayoutParams();
                layoutParams.height = val;
                mDashRL.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(1000l);
        anim.start();
    }*/

    @Override
    public void onStart() {
        super.onStart();
        rvExperts.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        requestLoadExperts(FilterDialogFragment.FilterEntity.HIGH_RATING);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    private void requestLoadExperts(String sort) {
        queue = Volley.newRequestQueue(getContext());
        long userID = AstrologyApplication.getInstance().getCurrentUser().getServerID();
        ExpertRequest expertRequest = ExpertRequest.loadExpertsByCategory(userID, categoryID, sort, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //progressBar.setVisibility(View.GONE);
                //rvServices.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    //parseResponse( jsonResponse.getString("data") );
                    parseResponse( jsonResponse );
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    Log.e(TAG, new String(error.networkResponse.data));
                }
            }
        });

        expertRequest.setTag(TAG);
        queue.add(expertRequest);
    }

    private void parseResponse(JSONObject jsonResponse) throws JSONException {

        String imageURL = jsonResponse.getString("image_url");
        String description = jsonResponse.getString("description");
        String jsonExperts = jsonResponse.getString("data");

        mDescriptionTV.setText(description);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(Constants.SERVER_PRODUCTION + imageURL, mIconIV);

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);

        Gson gson = builder.create();

        List<Expert> list = gson.fromJson(jsonExperts, new TypeToken<ArrayList<Expert>>() {}.getType());

        rvExperts.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);

        if (list.isEmpty()) {
            mEmptyListTV.setVisibility(View.VISIBLE);
        }

        if (expertsAdapter == null) {
            expertsAdapter = ExpertsAdapter.createFullDataAdapter(list, this);
            rvExperts.setAdapter(expertsAdapter);
        } else {
            expertsAdapter.clear();
            expertsAdapter.addData(list);
        }
    }

    @Override
    public void onItemInteraction(Expert expert) {

        if (expert.getDialog() != null) {
            Intent intent = MessagesActivity.createIntent(getContext(), expert.getDialog(), AstrologyApplication.getInstance().getCurrentUser(), expert.getUser());
            startActivity(intent);
            return;
        }

        ((ExpertsActivity)getActivity()).showProfileDialog(expert);
    }
}
