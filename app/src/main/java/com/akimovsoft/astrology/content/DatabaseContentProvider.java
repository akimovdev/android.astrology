package com.akimovsoft.astrology.content;

import com.activeandroid.Configuration;
import com.activeandroid.content.ContentProvider;
import com.akimovsoft.astrology.model.entity.City;
import com.akimovsoft.astrology.model.entity.User;


public class DatabaseContentProvider extends ContentProvider {

	@Override
	protected Configuration getConfiguration() {
		Configuration.Builder builder = new Configuration.Builder(getContext());
		builder.addModelClass(User.class);
		builder.addModelClass(City.class);
		return builder.create();
	}
}